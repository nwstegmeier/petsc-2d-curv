#include <petsc.h>
#include "xml.h"
#include <iostream>
#include <fstream>
using namespace std;

void save_PRect_ASCII(){
  int irank;
  ofstream gfile;
  char filename[PETSC_MAX_PATH_LEN];
  char pfilename[PETSC_MAX_PATH_LEN];
  PetscSNPrintf(filename,sizeof(filename),"YY%04i_%04i.vtr",rank,num_save);
  PetscSNPrintf(pfilename,sizeof(pfilename),"data/PVTK%04i.pvtr",num_save);
  if(rank==0){
    gfile.open(pfilename);
    gfile << "<?xml version=\"1.0\"?>\n";
    gfile << "<VTKFile type=\"PRectilinearGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
    gfile << "\t<PRectilinearGrid WholeExtent=\"1 " << nx << " 1 " << ny <<" 0 0\" GhostLevel=\"1\">\n";
    gfile << "\t\t<PPointData Scalars=\"flow data\">\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"rho\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"u\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"v\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"uxN\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"vyN\" format=\"ascii\"/>\n";
    gfile << "\t\t</PPointData>\n";
    gfile << "\t\t<PCoordinates>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"x\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"y\" format=\"ascii\"/>\n";
    gfile << "\t\t\t<PDataArray type=\"Float32\" Name=\"z\" format=\"ascii\"/>\n";
    gfile << "\t\t</PCoordinates>\n";
    gfile.close();
  }  
  for(irank=0; irank<size;irank++){
    if(irank==rank){
      gfile.open(pfilename,ios::out | ios::app);
      gfile << "\t\t<Piece Extent=\"" << global_firstx-1 << " " << global_lastx+1 << " " 
                                      << global_firsty-1 << " " << global_lasty+1 
                                      << " 0 0\" Source=\"" << filename << "\"/>\n";
      gfile.close();
    }
    MPI_Barrier(PETSC_COMM_WORLD);
  }
  if(rank==0){
    gfile.open(pfilename,ios::out | ios::app);
    gfile << "\t</PRectilinearGrid>\n";
    gfile << "</VTKFile>\n";
    gfile.close();
  }  
};

void save_Rect_ASCII(){
  int i,j;
  ofstream lfile;
  char filename[PETSC_MAX_PATH_LEN];
  PetscSNPrintf(filename,sizeof(filename),"data/YY%04i_%04i.vtr",rank,num_save);
  lfile.open(filename);
  lfile << "<?xml version=\"1.0\"?>\n";
  lfile << "<VTKFile type=\"RectilinearGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
  lfile << "\t<RectilinearGrid WholeExtent=\"1 " << nx << " 1 " << ny <<" 0 0\" GhostLevel=\"1\">\n";
  lfile << "\t\t<Piece Extent=\"" << global_firstx-1 << " " << global_lastx+1 << " " 
                                  << global_firsty-1 << " " << global_lasty+1 << " 0 0\">\n";
  lfile << "\t\t\t<PointData Scalars=\"flow data\">\n";
  
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"rho\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << rho[j][i][t] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"u\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << u[j][i][t] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"v\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << v[j][i][t] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"uxN\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << uxN[j][i][t] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"vyN\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << vyN[j][i][t] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  
  lfile << "\t\t\t</PointData>\n";
  lfile << "\t\t\t<Coordinates>\n";
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"x\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << x[j][i] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"y\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (i = firstx-1; i <= lastx+1; i++){
    for (j = firsty-1; j <= lasty+1; j++){
      lfile << y[j][i] << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  lfile << "\t\t\t\t<DataArray type=\"Float32\" Name=\"z\" format=\"ascii\">\n";
  lfile << "\t\t\t\t\t";
    for (j = firsty-1; j <= lasty+1; j++){
    for (i = firstx-1; i <= lastx+1; i++){
      lfile << 0.0 << " ";
    }
    }
  lfile << "\n";
  lfile << "\t\t\t\t</DataArray>\n";
  lfile << "\t\t\t</Coordinates>\n";
  lfile << "\t\t</Piece>\n";
  lfile << "\t</RectilinearGrid>\n";
  lfile << "</VTKFile>\n";
  lfile.close();
  
};

void save_legacy_ASCII(){
  FILE *file = NULL;
  char filename[PETSC_MAX_PATH_LEN];
  PetscInt i,j,k,zone;  
  PetscReal zp;
  
  if(num_save==0){
    zone=0;
  } else {
    zone=n/save_interval;
  }
  PetscSNPrintf(filename,sizeof(filename),"data/YY%04i.%04i.dat",rank,num_save);
  PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
  PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v,uxN,vyN\n");	
  PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\",I=%i J=%i K=4 F=POINT\n",zone,xw+2,yw+2);
  for (k = 0; k < 4; k++){
  for (j = firsty-1; j <= lasty+1; j++){
  for (i = firstx-1; i <= lastx+1; i++){
    zp=k*0.5+0.5/2.0;		
    PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",
      x[j][i],y[j][i],zp,rho[j][i][t],u[j][i][t],v[j][i][t],uxN[j][i][t],vyN[j][i][t]);
  }		
  }
  }

  fclose(file);
  file = NULL;
  num_save=num_save+1;
  
}   

void save_VTK_ASCII(){
  write_PRect_ASCII();
  write_Rect_ASCII();
}

