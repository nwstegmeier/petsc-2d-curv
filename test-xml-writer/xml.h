#ifndef _XML_H
#define _XML_H

#include "datastruct.h"

extern void save_PRect_ASCII();
extern void save_Rect_ASCII();
extern void save_VTK_ASCII();
extern void save_legacy_ASCII();

#endif