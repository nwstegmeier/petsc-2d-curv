#ifndef _DATASTRUCT_H
#define _DATASTRUCT_H

#include <petsc.h>

extern PetscMPIInt  rank,size;
extern PetscInt	    n;
extern PetscInt     nstart;
extern PetscInt     nstop;
extern PetscInt     save_interval;
extern PetscInt     num_save;
extern PetscInt	    yes_tecplot;
extern PetscInt     nx,ny;
extern PetscReal    dt;

extern PetscInt     xs,xw,ys,yw;
extern DM           da;
extern Mat          A;
extern Vec          bv,solv;
extern KSP          ksp;
extern PC           pcksp;
extern PetscInt     firstx,firsty,lastx,lasty;
extern PetscInt     global_firstx,global_firsty,global_lastx,global_lasty;
extern PetscReal    lx,ly,dx,dy;
extern PetscReal    sim_time;
extern PetscInt     t;
extern MPI_Datatype xface3D,yface3D,xface2D,yface2D;
extern PetscMPIInt  nbrleft,nbrright,nbrbottom,nbrtop; 
extern PetscReal    max_drho_global,max_dgx_global,max_dgy_global;
extern PetscInt     not_converged;
extern PetscReal    ksp_tol,tolerance_drho;

extern PetscReal    rho_in,rho_bot,rho_top,rho_left,rho_right;
extern PetscReal    BCvarxLc,BCvaryLc,BCvarxRc,BCvaryRc;

extern double       *xMem,*yMem,*rhoMem,*uMem,*vMem,*uxNMem,*vyNMem;
extern double       *rhosMem;
extern double       **x,**y,***rho,***u,***v,***uxN,***vyN;
extern double       **rhos;
extern double       **b,**sol;

#endif