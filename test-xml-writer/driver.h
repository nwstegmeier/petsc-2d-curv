#ifndef _DRIVER_H
#define _DRIVER_H

#include "datastruct.h"

extern void solve_NSCH();

extern void set_up_Petsc();

extern void init_cond();

extern void advance_continuity();

extern void advance_gx();

extern void advance_gy();

extern void save_data();

extern void enforce_ghost_by_bc();

extern void time_forward();

extern void compute_face_vel();

extern void update_ghost_3D(double ***a,int t_idx);

extern void update_ghost_2D(double **a);

extern void store_current_ts(double **as, double ***a);

extern void check_convergence();

extern void update_bc();

extern void set_boundary_values();

#endif