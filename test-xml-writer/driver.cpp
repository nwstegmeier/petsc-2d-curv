#include "driver.h"
#include "xml.h"
#include <petsc.h>

void solve_NSCH(){
  PetscInt iter_main_loop;
  
	init_cond();
  enforce_ghost_by_bc();
  save_VTK_ASCII();
  
	for (n=nstart; n<nstop+1; n++){		
    iter_main_loop = 0;
    
    while (not_converged){
      iter_main_loop += 1;
      
      advance_continuity();
      
      check_convergence(); 
      
      PetscPrintf(PETSC_COMM_WORLD,"%5i %5i %12.4e\n",n,iter_main_loop,max_drho_global);
      
    }
    
		if(n%save_interval==0){
      save_VTK_ASCII();
      num_save=num_save+1;
    }	
    
    time_forward();
    not_converged=1;
    
	}
  
};

void set_up_Petsc(){
  const PetscMPIInt   *neighbors;
	DMBoundaryType 		  bx=DM_BOUNDARY_PERIODIC;
	DMBoundaryType 		  by=DM_BOUNDARY_PERIODIC;
	DMDAStencilType 	  stype=DMDA_STENCIL_BOX;
	PetscInt 			      dof=1,sw=1,i,j;
			
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,dof,sw,NULL,NULL,&da);
  DMSetFromOptions(da);
  DMSetUp(da);
	DMDAGetCorners(da,&xs,&ys,0,&xw,&yw,0);
	DMView(da,PETSC_VIEWER_STDOUT_WORLD);
  
  DMCreateGlobalVector(da,&bv);
  DMCreateGlobalVector(da,&solv);	
  VecSetUp(bv); 
  VecSetUp(solv);
  VecSetFromOptions(bv); 
  VecSetFromOptions(solv); 
  
  DMCreateMatrix(da,&A);
	MatSetFromOptions(A);
	MatSetUp(A);

  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetType(ksp,KSPGMRES);
  KSPGetPC(ksp,&pcksp);
  PCSetType(pcksp,PCJACOBI);
  KSPSetPC(ksp,pcksp);
  KSPSetFromOptions(ksp);
  KSPSetOperators(ksp,A,A);
  KSPSetUp(ksp);
  
  KSPSetTolerances(ksp,ksp_tol,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
  
  // determine neighboring processors
  DMDAGetNeighbors(da,&neighbors);
  nbrleft		= neighbors[3];
  nbrright	= neighbors[5];
  nbrbottom	= neighbors[1];
  nbrtop		= neighbors[7];
  if(nbrleft<0){nbrleft=MPI_PROC_NULL;}
  if(nbrright<0){nbrright=MPI_PROC_NULL;}
  if(nbrbottom<0){nbrbottom=MPI_PROC_NULL;}
  if(nbrtop<0){nbrtop=MPI_PROC_NULL;}
	
  // define the "interior" domain
  firstx=1; 
  firsty=1;
  lastx=xw;
  lasty=yw;
  // define location in global domain
  global_firstx = xs+1;
  global_lastx  = xs+xw;
  global_firsty = ys+1;
  global_lasty  = ys+yw;
  
  // allocate x,y
  xMem    = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  yMem    = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  rhosMem = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  x       = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  y       = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  rhos    = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  for(j=firsty-1; j<=lasty+1; j++){
    x[j]     = xMem + (lastx-firstx+3)*j;
    y[j]     = yMem + (lastx-firstx+3)*j;
    rhos[j]  = rhosMem + (lastx-firstx+3)*j;
  }
  
  // allocate 3D PDE variables
  rhoMem  = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  uMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  vMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  uxNMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  vyNMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  rho = new double**[lasty-firsty+3];
  u   = new double**[lasty-firsty+3];
  v   = new double**[lasty-firsty+3];
  uxN = new double**[lasty-firsty+3];
  vyN = new double**[lasty-firsty+3];
  
  for(j=firsty-1; j<=lasty+1; j++){
    rho[j]  = new double*[lastx-firstx+3];
    u[j]    = new double*[lastx-firstx+3];
    v[j]    = new double*[lastx-firstx+3];
    uxN[j]  = new double*[lastx-firstx+3];
    vyN[j]  = new double*[lastx-firstx+3];
    for (i=firstx-1; i<=lastx+1; i++){
      rho[j][i] = rhoMem + j*(lastx-firstx+3)*3 + i*3;
      u[j][i]   = uMem   + j*(lastx-firstx+3)*3 + i*3;
      v[j][i]   = vMem   + j*(lastx-firstx+3)*3 + i*3;
      uxN[j][i] = uxNMem + j*(lastx-firstx+3)*3 + i*3;
      vyN[j][i] = vyNMem + j*(lastx-firstx+3)*3 + i*3;
    }
  }
  
  // create custom MPI data-type for exchanging xface boundaries
  MPI_Type_vector(lasty-firsty+3,1,(lastx-firstx+3),MPI_DOUBLE_PRECISION,&xface2D);
  MPI_Type_commit(&xface2D);
  MPI_Type_vector(lasty-firsty+3,1,(lastx-firstx+3)*3,MPI_DOUBLE_PRECISION,&xface3D);
  MPI_Type_commit(&xface3D);
  MPI_Type_vector((lastx-firstx+3),1,3,MPI_DOUBLE_PRECISION,&yface3D);
  MPI_Type_commit(&yface3D);
  
};

void init_cond(){
  PetscInt i,j;
  
  lx = 2.0*PETSC_PI;
  ly = 2.0*PETSC_PI;
  dx = lx/nx;
  dy = ly/ny;
  n=1;
  sim_time = 0.0;
  
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    x[j][i]= -dx/2.0 + (float)(i+xs)*dx;
    y[j][i]= -dy/2.0 + (float)(j+ys)*dy;
    rho[j][i][t] = PetscSinReal(x[j][i]);
    u[j][i][t] = 1.0;
    v[j][i][t] = 0.0;
  }
  }
      
  PetscPrintf(PETSC_COMM_WORLD,"\n---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"-------------- Sim Params -------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"rank=%d, size=%d\n",rank,size);
  PetscPrintf(PETSC_COMM_WORLD,"nstart=%i,nstop=%i,save_interval=%i\n",nstart,nstop,save_interval);
  PetscPrintf(PETSC_COMM_WORLD,"num_save=%i,yes_tecplot=%i\n",num_save,yes_tecplot);
  PetscPrintf(PETSC_COMM_WORLD,"nx=%d,ny=%d\n",nx,ny);
  PetscPrintf(PETSC_COMM_WORLD,"lx=%f,ly=%f\n",lx,ly);
  PetscPrintf(PETSC_COMM_WORLD,"dx=%f,dy=%f\n",dx,dy);
  PetscPrintf(PETSC_COMM_WORLD,"dt=%f\n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"Initial conditions saved... \n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n\n");
  
};

void advance_continuity(){
  PetscInt	  i,j,ii,jj;
  PetscReal	  apf,apbf,aexf,awxf,aeyf,awyf,rightf,vals[5];
  PetscReal   bpf,brsf,bexf,bwxf,beyf,bwyf;
  MatStencil 	row,col[5];
  
  store_current_ts(rhos,rho);
  
  DMDAVecGetArray(da,bv,&b);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs-1;
    jj=j+ys-1;
    
    apf  = 1.0 + 0.25*dt*(0.0 + ( uxN[j  ][i+1][t] - uxN[j][i][t] )/dx
                              + ( vyN[j+1][i  ][t] - vyN[j][i][t] )/dy );
                            
    aexf =  0.25*dt*uxN[j  ][i+1][t]/dx;
    awxf = -0.25*dt*uxN[j  ][i  ][t]/dx;
    aeyf =  0.25*dt*vyN[j+1][i  ][t]/dy;
    awyf = -0.25*dt*vyN[j  ][i  ][t]/dy;
    
    bpf  = 1.0 - 0.25*dt*( 0.0 + ( uxN[j  ][i+1][t] - uxN[j][i][t] )/dx 
                               + ( vyN[j+1][i  ][t] - vyN[j][i][t] )/dy );
                             
    bexf = -0.25*dt*uxN[j  ][i+1][t]/dx;
    bwxf =  0.25*dt*uxN[j  ][i  ][t]/dx;
    beyf = -0.25*dt*vyN[j+1][i  ][t]/dy;
    bwyf =  0.25*dt*vyN[j  ][i  ][t]/dy;
    
    brsf = 0.0;
    apbf = 0.0;
    
    rightf = bpf*rho[j][i][t-1]
            + bexf*rho[j  ][i+1][t-1] + bwxf*rho[j  ][i-1][t-1]
            + beyf*rho[j+1][i  ][t-1] + bwyf*rho[j-1][i  ][t-1];
              
    vals[0] = apf;
    vals[1] = awxf;
    vals[2] = aexf;
    vals[3] = awyf;
    vals[4] = aeyf;
    b[jj][ii] = rightf;
  
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = ii  ;    row.j = jj  ; // matrix row
    col[0].i = ii  ; col[0].j = jj  ; // ap
    col[1].i = ii-1; col[1].j = jj  ; // awx
    col[2].i = ii+1; col[2].j = jj  ; // aex
    col[3].i = ii  ; col[3].j = jj-1; // awy 
    col[4].i = ii  ; col[4].j = jj+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
    
  }
  }
  
  DMDAVecRestoreArray(da,bv,&b);
  
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
 
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  
  KSPSolve(ksp,bv,solv);
  MatZeroEntries(A);
  
  DMDAVecGetArray(da,solv,&sol);
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs;
    jj=j+ys;
    rho[j][i][t]=sol[jj-1][ii-1];
    //PetscPrintf(PETSC_COMM_SELF,"rho[%i][%i] %f. \n",j,i,rho[j][i]);  
  }
  }
  DMDAVecRestoreArray(da,solv,&sol);
    
  update_ghost_3D(rho,t);
  
}

void update_ghost_3D(double ***a, int t_idx = 2){ 
  MPI_Status status; 

  //--------------------- 3D Array Case --------------------------------
  // exchange ghost points on y faces
  MPI_Sendrecv(&a[lasty   ][firstx-1][t_idx],1,yface3D,nbrtop   ,0, 
               &a[firsty-1][firstx-1][t_idx],1,yface3D,nbrbottom,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty  ][firstx-1][t_idx],1,yface3D,nbrbottom,1, 
               &a[lasty+1 ][firstx-1][t_idx],1,yface3D,nbrtop   ,1, 
               PETSC_COMM_WORLD,&status);


  // exchange ghost points on x faces
  MPI_Sendrecv(&a[firsty-1][lastx   ][t_idx],1,xface3D,nbrright,0, 
               &a[firsty-1][firstx-1][t_idx],1,xface3D,nbrleft ,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty-1][firstx  ][t_idx],1,xface3D,nbrleft ,1, 
               &a[firsty-1][lastx+1 ][t_idx],1,xface3D,nbrright,1, 
               PETSC_COMM_WORLD,&status);

  MPI_Barrier(PETSC_COMM_WORLD);
               
}

void update_ghost_2D(double **a){ 
  MPI_Status status; 

  //--------------------- 2D Array Case --------------------------------  
  // exchange ghost points on y faces
  MPI_Sendrecv(&a[lasty   ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrtop   ,0,  \
               &a[firsty-1][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrbottom,0,  \
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty  ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrbottom,1,  \
               &a[lasty+1 ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrtop   ,1,  \
               PETSC_COMM_WORLD,&status);


  // exchange ghost points on x faces
  MPI_Sendrecv(&a[firsty-1][lastx   ],1,xface2D,nbrright,0, 
               &a[firsty-1][firstx-1],1,xface2D,nbrleft ,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty-1][firstx  ],1,xface2D,nbrleft ,1, 
               &a[firsty-1][lastx+1 ],1,xface2D,nbrright,1, 
               PETSC_COMM_WORLD,&status);  

  MPI_Barrier(PETSC_COMM_WORLD);
               
}             

void enforce_ghost_by_bc(){
  PetscInt i,j,k;
  
  // outer_bc?
  
  update_ghost_3D(rho,t);
  update_ghost_3D(u,t);
  update_ghost_3D(v,t);
  
  compute_face_vel();
  
  update_ghost_3D(uxN,t);
  update_ghost_3D(vyN,t);
  
  // initial time_forward
  for(k=2; k>=1; k--){
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    rho[j][i][k-1] = rho[j][i][k];
    u[j][i][k-1] = u[j][i][k];
    v[j][i][k-1] = v[j][i][k];
    uxN[j][i][k-1] = uxN[j][i][k];
    vyN[j][i][k-1] = vyN[j][i][k];
  }
  }
  }
    
}

void time_forward(){
  PetscInt i,j,k;
  for(k=2; k>=1; k--){
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    rho[j][i][k-1] = rho[j][i][k];
    u[j][i][k-1] = u[j][i][k];
    v[j][i][k-1] = v[j][i][k];
    uxN[j][i][k-1] = uxN[j][i][k];
    vyN[j][i][k-1] = vyN[j][i][k];
  }
  }
  }
  
  sim_time+=dt;
  
}

void compute_face_vel(){
  PetscInt i,j;
  for(j=firsty; j<=lasty+1; j++){
  for(i=firstx; i<=lastx+1; i++){
    uxN[j][i][t] = 0.5*(u[j  ][i-1][t]+u[j][i][t]);
    vyN[j][i][t] = 0.5*(v[j-1][i  ][t]+v[j][i][t]);
  }
  }
}

void save_data(){
  FILE *file = NULL;
  char filename[PETSC_MAX_PATH_LEN];
  PetscInt i,j,k,zone;  
  PetscReal zp;
    
  if(num_save==0){
    zone=0;
  } else {
    zone=n/save_interval;
  }
  PetscSNPrintf(filename,sizeof(filename),"data/YY%04i.%04i.dat",rank,num_save);
  PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
  PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v,uxN,vyN\n");	
  PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\",I=%i J=%i K=4 F=POINT\n",zone,xw+2,yw+2);
  for (k = 0; k < 4; k++){
  for (j = firsty-1; j <= lasty+1; j++){
  for (i = firstx-1; i <= lastx+1; i++){
    zp=k*0.5+0.5/2.0;		
    PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",
      x[j][i],y[j][i],zp,rho[j][i][t],u[j][i][t],v[j][i][t],uxN[j][i][t],vyN[j][i][t]);
  }		
  }
  }

  fclose(file);
  file = NULL;
  
}      

void store_current_ts(double **as, double ***a){
  PetscInt i,j;
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    as[j][i]=a[j][i][t];
  }
  }
}

void check_convergence(){
  PetscInt  i,j;
  PetscReal max_drho_local;
  
  max_drho_local = 0.0;
  
  for(j=firsty; j<=lasty; j++){
  for(i=firstx; i<=lastx; i++){
    max_drho_local = PetscMax(max_drho_local,PetscAbsReal(rho[j][i][t]-rhos[j][i]));
  }
  }
  
  MPI_Allreduce(&max_drho_local,&max_drho_global,1,MPI_DOUBLE_PRECISION,MPI_MAX,PETSC_COMM_WORLD);
  
  if(max_drho_global < tolerance_drho){
    not_converged=0; // not not converged -> converged
  }
  MPI_Barrier(PETSC_COMM_WORLD);
    
}

// requires rho_in, rho_bot...
void update_bc(){
  
}

// function to build rho_in, rho_bot...
void set_boundary_values(){
  
}



