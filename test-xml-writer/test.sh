#!/bin/bash

nstart=0
nstop=1
saveint=1
num_save=0
yes_tecplot=0
dt=0.001
nx=20
ny=20

mpiexec -n 1 ./run -nx $nx -ny $ny -dt $dt -nstart $nstart -nstop $nstop -saveint $saveint -num_save $num_save -yes_tecplot $yes_tecplot -tolerance_drho 0.000001 -ksp_tol 0.000001

exit
