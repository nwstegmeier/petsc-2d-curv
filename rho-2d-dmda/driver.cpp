#include "driver.h"
#include <petsc.h>

void set_up_Petsc(){
	DMBoundaryType 		bx=DM_BOUNDARY_PERIODIC;
	DMBoundaryType 		by=DM_BOUNDARY_PERIODIC;
	DMDAStencilType 	stype=DMDA_STENCIL_BOX;
	PetscInt 			dof = 3,sw = 1, ione=1;
			
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,dof,sw,NULL,NULL,&da);
  DMSetFromOptions(da);
  DMSetUp(da);
	DMCreateGlobalVector(da,&gv);
	DMCreateLocalVector(da,&lv);
	DMDAGetCorners(da,&xs,&ys,0,&xw,&yw,0);
	DMView(da,PETSC_VIEWER_STDOUT_WORLD);
	
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,ione,sw,NULL,NULL,&daKSP);
  DMSetFromOptions(daKSP);
  DMSetUp(daKSP);
	DMCreateGlobalVector(daKSP,&bv);
	DMCreateGlobalVector(daKSP,&X);
	DMCreateLocalVector(daKSP,&lX);
  VecSetUp(bv); VecSetUp(X); VecSetUp(lX);
  VecSetFromOptions(bv); VecSetFromOptions(X); VecSetFromOptions(lX);
	DMCreateMatrix(daKSP,&A);
	MatSetFromOptions(A);
	MatSetUp(A);
	
	VecCreateMPI(PETSC_COMM_SELF,(yw+2)*(xw+2),(ny+2)*(nx+2),&xv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+2)*(xw+2),(ny+2)*(nx+2),&yv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+1)*(xw+1),(ny+1)*(nx+1),&uxNv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+1)*(xw+1),(ny+1)*(nx+1),&vyNv);
	
	KSPCreate(PETSC_COMM_WORLD,&ksp);
	KSPSetType(ksp,KSPGMRES);
	KSPGetPC(ksp,&pcksp);
	PCSetType(pcksp,PCJACOBI);
	KSPSetPC(ksp,pcksp);
	KSPSetFromOptions(ksp);
	KSPSetOperators(ksp,A,A);
	KSPSetUp(ksp);
		
};

void solve_NSKH(){
	
  // set the initial conditions
	init_cond();
	
  // time evolution
	for (n=nstart; n<nstop; n++){
		
		advance_continuity();
		
		if(n%save_interval==0){
			save_data();
		}
		
		sim_time += dt;
		
	}
	
};

void init_cond(){
  PetscInt i,j;
  
  lx = 2.0*PETSC_PI;
  ly = 2.0*PETSC_PI;
  dx = lx/nx;
  dy = ly/ny;
  n=1;
  sim_time = 0.0;
  
  VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
  VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
  DMDAVecGetArray(da,lv,&L);
  for (j = ys-1; j < ys+yw+1; j++){ 
  for (i = xs-1; i < xs+xw+1; i++){ 
  	x[j][i]=(i+1)*dx+dx/2.0;
  	y[j][i]=(j+1)*dy+dy/2.0;
  	L[j][i].d = PetscSinReal(x[j][i]);	
  	L[j][i].u = 1.0;
  	L[j][i].v = 0.0;
  	//PetscPrintf(PETSC_COMM_WORLD,"x=%f, G[%d].d=%f\n",G[i].x,i,G[i].d);
  }
  }
  VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
  VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
  DMDAVecRestoreArray(da,lv,&L);
  VecAssemblyBegin(lv);
  VecAssemblyEnd(lv);
  	
  VecGetArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
  VecGetArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
  DMDAVecGetArray(da,lv,&L);
  for (j = ys; j < ys+yw+1; j++){ 
  for (i = xs; i < xs+xw+1; i++){
  	uxN[j][i]=0.5*(L[j  ][i-1].u+L[j][i].u);
  	vyN[j][i]=0.5*(L[j-1][i  ].v+L[j][i].v);
  }
  }
  VecRestoreArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
  VecRestoreArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
  DMDAVecRestoreArray(da,lv,&L);
  
  save_data();

  PetscPrintf(PETSC_COMM_WORLD,"\n---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"-------------- Sim Params -------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"rank=%d, size=%d\n",rank,size);
  PetscPrintf(PETSC_COMM_WORLD,"nstart=%i,nstop=%i,save_interval=%i\n",nstart,nstop,save_interval);
  PetscPrintf(PETSC_COMM_WORLD,"num_save=%i,yes_tecplot=%i\n",num_save,yes_tecplot);
  PetscPrintf(PETSC_COMM_WORLD,"nx=%d,ny=%d\n",nx,ny);
  PetscPrintf(PETSC_COMM_WORLD,"lx=%f,ly=%f\n",lx,ly);
  PetscPrintf(PETSC_COMM_WORLD,"dx=%f,dy=%f\n",dx,dy);
  PetscPrintf(PETSC_COMM_WORLD,"dt=%f\n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"Initial conditions saved... \n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n\n");
	
};

void advance_continuity(){
	PetscInt	i,j;
	PetscReal	ap,aex,awx,aey,awy,rhs,vals[5];
	MatStencil 	row,col[5];

	DMDAVecGetArray(da,lv,&L);
	DMDAVecGetArray(daKSP,bv,&b);
	VecGetArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
	VecGetArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
	PetscMemzero(col,5*sizeof(MatStencil));
	
	for (j = ys; j < ys+yw; j++){
	for (i = xs; i < xs+xw; i++){
		
			ap  = 1 + 0.25*( (dt/dx)*(uxN[j][i+1]-uxN[j][i]) + (dt/dy)*(vyN[j+1][i]-vyN[j][i]) );
			awx =    -0.25*(dt/dx)*uxN[j  ][i  ];
			aex =     0.25*(dt/dx)*uxN[j  ][i+1];
			awy =    -0.25*(dt/dy)*vyN[j  ][i  ];
			aey =     0.25*(dt/dy)*vyN[j+1][i  ];	
			rhs =   L[j][i].d*(1.+0.25*( (dt/dx)*(uxN[j][i]-uxN[j][i+1]) + (dt/dy)*(vyN[j][i]-vyN[j+1][i]) ) ) \
						- L[j  ][i+1].d*(0.25*(dt/dx)*uxN[j  ][i+1]) \
						+ L[j  ][i-1].d*(0.25*(dt/dx)*uxN[j  ][i  ]) \
						- L[j+1][i  ].d*(0.25*(dt/dy)*vyN[j+1][i  ]) \
						+ L[j-1][i  ].d*(0.25*(dt/dy)*vyN[j  ][i  ]);
						
			vals[0] = ap;
			vals[1] = awx;
			vals[2] = aex;
			vals[3] = awy;
			vals[4] = aey;
			b[j][i] = rhs;
			
			// Specify matrix location of elements using (i,j) location in grid	
			row.i    = i  ;    row.j = j  ; // matrix row
			col[0].i = i  ; col[0].j = j  ; // ap
			col[1].i = i-1; col[1].j = j  ; // awx
			col[2].i = i+1; col[2].j = j  ; // aex
			col[3].i = i  ; col[3].j = j-1; // awy 
			col[4].i = i  ; col[4].j = j+1; // aey
			MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
			
	}
	}
  		
	DMDAVecRestoreArray(da,lv,&L);
	DMDAVecRestoreArray(daKSP,bv,&b);
	VecRestoreArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
	VecRestoreArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
	
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
	KSPSolve(ksp,bv,X);

	DMGlobalToLocalBegin(daKSP,X,INSERT_VALUES,lX);
	DMGlobalToLocalEnd(daKSP,X,INSERT_VALUES,lX);
	VecStrideScatter(lX,0,lv,INSERT_VALUES);   // scatter into density slot
	MatZeroEntries(A);
	
};

void advance_gx(){
  PetscInt	i,j;
  PetscReal	ap,aex,awx,aey,awy,rhs,vals[5];
  MatStencil 	row,col[5];
  
	DMDAVecGetArray(da,lv,&L);
	DMDAVecGetArray(daKSP,bv,&b);
	VecGetArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
	VecGetArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
	PetscMemzero(col,5*sizeof(MatStencil));
	
	for (j = ys; j < ys+yw; j++){
	for (i = xs; i < xs+xw; i++){
    
	}
	}
  		
	DMDAVecRestoreArray(da,lv,&L);
	DMDAVecRestoreArray(daKSP,bv,&b);
	VecRestoreArray2d(uxNv,(yw+1),(xw+1),ys,xs,&uxN);
	VecRestoreArray2d(vyNv,(yw+1),(xw+1),ys,xs,&vyN);
 
}

void advance_gy(){
}

void save_data(){
	FILE *file = NULL;
	char filename[PETSC_MAX_PATH_LEN];
	PetscInt i,j,k,zone;  
	PetscReal zp;
	//PetscViewer viewer;
	if(num_save==0){
		zone=0;
	} else {
		zone=n/save_interval;
	}
	if(yes_tecplot==1){
		PetscSNPrintf(filename,sizeof(filename),"data/XX%04i.%04i.dat",num_save,rank);
		PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
		PetscFPrintf(PETSC_COMM_SELF,file,"Title=\"Jet\"\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\", STRANDID=1, SOLUTIONTIME=%f I=%i J=%i K=4 F=POINT\n",zone,sim_time,xw+2,yw+2);
		
		DMDAVecGetArray(da,lv,&L);
		VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
		VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
		for (k = 0; k < 4; k++){
		for (j = ys-1; j < ys+yw+1; j++){
		for (i = xs-1; i < xs+xw+1; i++){
			zp=k*0.5+0.5/2.0;		
			PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",x[j][i],y[j][i],zp,L[j][i].d,L[j][i].u,L[j][i].v);
		}		
		}
		}
		DMDAVecRestoreArray(da,lv,&L);
		VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
		VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
		fclose(file);
		file = NULL;
	}
	if(yes_tecplot==0){
		PetscSNPrintf(filename,sizeof(filename),"data/YY%04i.%04i.dat",rank,num_save);
		PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
		PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\",I=%i J=%i K=4 F=POINT\n",zone,xw+2,yw+2);
		
		DMDAVecGetArray(da,lv,&L);
		VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
		VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
		for (k = 0; k < 4; k++){
		for (j = ys-1; j < ys+yw+1; j++){
		for (i = xs-1; i < xs+xw+1; i++){
			zp=k*0.5+0.5/2.0;		
			PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",x[j][i],y[j][i],zp,L[j][i].d,L[j][i].u,L[j][i].v);
		}		
		}
		}
		DMDAVecRestoreArray(da,lv,&L);
		VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs+1,&x);
		VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs+1,&y);
		fclose(file);
		file = NULL;
	}
	num_save=num_save+1;
};

