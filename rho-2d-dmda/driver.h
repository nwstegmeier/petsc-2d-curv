#ifndef _DRIVER_H
#define _DRIVER_H

#include "datastruct.h"

extern void set_up_Petsc();

extern void solve_NSKH();

extern void init_cond();

extern void advance_continuity();

extern void advance_gx();

extern void advance_gy();

extern void save_data();

#endif