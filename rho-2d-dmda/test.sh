#!/bin/bash

nstart=0
nstop=2
saveint=1
num_save=0
yes_tecplot=0
dt=0.01
nx=4
ny=4

mpiexec -n 1 ./run -nx $nx -ny $ny -dt $dt -nstart $nstart -nstop $nstop -saveint $saveint -num_save $num_save -yes_tecplot $yes_tecplot
