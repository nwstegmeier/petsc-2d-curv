#include <petsc.h>
#include "datastruct.h"
#include "driver.h"

// read in from test.sh file
PetscMPIInt   rank,size;
PetscInt		  n;
PetscInt		  nstart;
PetscInt 		  nstop;
PetscInt 		  save_interval;
PetscInt 		  num_save;
PetscInt		  yes_tecplot;
PetscInt 		  nx,ny;
PetscReal     dt;

// internally defined
PetscInt     xs,xw,ys,yw;
DM           da;
Mat          A;
Vec          bv,solv;
KSP          ksp;
PC           pcksp;
PetscInt     firstx,firsty,lastx,lasty;
PetscInt     global_firstx,global_firsty;
PetscInt     global_lastx,global_lasty;
PetscReal    lx,ly,dx,dy;
PetscReal    sim_time;
PetscInt     t=2;
MPI_Datatype xface3D,yface3D,xface2D,yface2D;
PetscMPIInt  nbrleft,nbrright,nbrbottom,nbrtop; 
double       *xMem,*yMem,*rhoMem,*uMem,*vMem,*uxNMem,*vyNMem;
double       **x,**y,***rho,***u,***v,***uxN,***vyN;
double       **b,**sol;

int main(int argc,char **argv){
	
	PetscInitialize(&argc,&argv,(char*)0,NULL);
	MPI_Comm_size(PETSC_COMM_WORLD,&size);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	
	PetscOptionsGetInt(NULL,NULL,"-nstart",&nstart,NULL);
	PetscOptionsGetInt(NULL,NULL,"-nstop",&nstop,NULL);
	PetscOptionsGetInt(NULL,NULL,"-saveint",&save_interval,NULL);
	PetscOptionsGetInt(NULL,NULL,"-num_save",&num_save,NULL);
	PetscOptionsGetInt(NULL,NULL,"-yes_tecplot",&yes_tecplot,NULL);
	PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);
	PetscOptionsGetInt(NULL,NULL,"-nx",&nx,NULL);
	PetscOptionsGetInt(NULL,NULL,"-ny",&ny,NULL);
	
	set_up_Petsc();
	solve_NSKH();
	
	PetscPrintf(PETSC_COMM_WORLD,"Simulation complete.\n\n"); 
  PetscFinalize();
		
}


