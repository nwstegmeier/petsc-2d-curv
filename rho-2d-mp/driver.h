#ifndef _DRIVER_H
#define _DRIVER_H

#include "datastruct.h"

extern void solve_NSKH();

extern void set_up_Petsc();

extern void init_cond();

extern void advance_continuity();

extern void advance_gx();

extern void advance_gy();

extern void save_data();

extern void enforce_ghost_by_bc();

extern void time_forward();

extern void compute_face_vel();

extern void update_ghost_3D(double ***a,int t_idx);

extern void update_ghost_2D(double **a);

#endif