function advance_gx()

    global A b gx u v uxN vyN d p nx ny dt dx dy Re

    A = zeros(nx*ny,nx*ny);
    b = zeros(nx*ny,1);
    
    for j=2:ny+1
        for i=2:nx+1
            
            ap  =  1.0 + 0.25*(dt/dx)*(0.5*(uxN(j,i+1,2)+uxN(j,i+1,1))-0.5*(uxN(j,i,2)+uxN(j,i,1))) ...
                       + 0.25*(dt/dy)*(0.5*(vyN(j+1,i,2)+vyN(j+1,i,1))-0.5*(vyN(j,i,2)+vyN(j,i,1)));
            aex =  0.25*(dt/dx)*(0.5*(uxN(j  ,i+1,2)+uxN(j  ,i+1,1)));
            awx = -0.25*(dt/dx)*(0.5*(uxN(j  ,i  ,2)+uxN(j  ,i  ,1)));
            aey =  0.25*(dt/dy)*(0.5*(vyN(j+1,i  ,2)+vyN(j+1,i  ,1)));
            awy = -0.25*(dt/dy)*(0.5*(vyN(j  ,i  ,2)+vyN(j  ,i  ,1)));
            
            bp  =  1.0 - 0.25*(dt/dx)*(0.5*(uxN(j,i+1,2)+uxN(j,i+1,1))-0.5*(uxN(j,i,2)+uxN(j,i,1))) ...
                       - 0.25*(dt/dy)*(0.5*(vyN(j+1,i,2)+vyN(j+1,i,1))-0.5*(vyN(j,i,2)+vyN(j,i,1)));
            bex = -0.25*(dt/dx)*(0.5*(uxN(j  ,i+1,2)+uxN(j  ,i+1,1)));
            bwx =  0.25*(dt/dx)*(0.5*(uxN(j  ,i  ,2)+uxN(j  ,i  ,1)));
            bey = -0.25*(dt/dy)*(0.5*(vyN(j+1,i  ,2)+vyN(j+1,i  ,1)));
            bwy =  0.25*(dt/dy)*(0.5*(vyN(j  ,i  ,2)+vyN(j  ,i  ,1)));
            
            rhs =   bp*gx(j,i) ...
                  + bex*gx(j,i+1) + bwx*gx(j,i-1) ...
                  + bey*gx(j+1,i) + bwy*gx(j-1,i) ...      
                  - 0.5*(dt/dx)*( p(j,i+1)-p(j,i-1) ) ...
                  + (dt/Re)*( u(j,i-1)-2*u(j,i)+u(j,i+1) )/(dx*dx) ...
                  + (dt/Re)*( u(j-1,i)-2*u(j,i)+u(j+1,i) )/(dy*dy);
            
            set_matrix_rhs(i,j,ap,aex,awx,aey,awy,rhs,0);
                        
        end
    end
    
    sol = A\b;
    
%     gx(2:ny+1,2:nx+1) = reshape(A\b,[nx ny])';
%     
    for j=1:ny
        for i=1:nx
            m = (j-1)*nx+i;
            gx(j+1,i+1) = sol(m);
        end
    end
     
    gx = update_outer_bc(gx); 
    
    u(:,:) = gx(:,:)./d(:,:);

end

