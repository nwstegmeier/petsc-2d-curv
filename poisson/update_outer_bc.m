function ff = update_outer_bc( fin )

    global nx ny;

    fin(:   ,   1) = fin(:   ,nx+1);
    fin(:   ,nx+2) = fin(:   ,   2);
    fin(1   ,   :) = fin(ny+1,   :);
    fin(ny+2,   :) = fin(2   ,   :);
    
    ff = fin;
    
end

