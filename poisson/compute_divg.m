function compute_divg()

    global D D2x D2y u v nx ny dx dy 

    for j = 2:ny+1
        for i =2:nx+1
            dux(j,i) = (u(j,i+1)-u(j,i-1))/(2*dx);
            dvy(j,i) = (v(j+1,i)-v(j-1,i))/(2*dy);
            D(j,i) = dux(j,i)+dvy(j,i);
        end
    end
    
    D = update_outer_bc(D);
    
    for j = 2:ny+1
        for i = 2:nx+1
            D2x(j,i) = (D(j,i+1)-2*D(j,i)+D(j,i-1))/(dx^2);
            D2y(j,i) = (D(j+1,i)-2*D(j,i)+D(j-1,i))/(dy^2);
        end
    end
    
    D2x = update_outer_bc(D2x);
    D2y = update_outer_bc(D2y);
        
%     % i=1 boundary
%     for j=2:ny+1
%         i=1;
%         dux = (u(j,i+1)-u(j,i))/dx;
%         dvy = (v(j+1,i)-v(j-1,i))/(2*dy);
%         D(j,i) = dux+dvy;
%     end
%     
%     % j=1 boundary
%     for i=2:nx+1
%         j=1;
%         dux = (u(j,i+1)-u(j,i-1))/(2*dx);
%         dvy = (v(j+1,i)-v(j,i))/dy;
%         D(j,i) = dux+dvy;
%     end
%     
%     % i=nx+2 boundary
%     for j=2:ny+1
%         i=nx+2;
%         dux = (u(j,i)-u(j,i-1))/dx;
%         dvy = (v(j+1,i)-v(j-1,i))/(2*dy);
%         D(j,i) = dux+dvy;
%     end
%     
%     % j=ny+2 boundary
%     for i=2:nx+1
%         j=ny+2;
%         dux = (u(2,i+1)-u(j,i-1))/(2*dx);
%         dvy = (v(j,i)-v(j-1,i))/dy;
%         D(j,i) = dux+dvy;
%     end
%     
%     %1,1
%     i=1; j=1;
%     dux = (u(j,i+1)-u(j,i))/dx;
%     dvy = (v(j+1,i)-v(j,i))/dy;
%     D(j,i) = dux+dvy;
%     
%     %1,ny+2
%     i=1; j=ny+2;
%     dux = (u(j,i+1)-u(j,i))/dx;
%     dvy = (v(j,i)-v(j-1,i))/dy;
%     D(j,i) = dux+dvy;
%     
%     %nx+2,1
%     i=nx+2; j=1;
%     dux = (u(j,i)-u(j,i-1))/dx;
%     dvy = (v(j+1,i)-v(j,i))/dy;
%     D(j,i) = dux+dvy;
%     
%     %nx+2,1
%     i=nx+2; j=ny+2;
%     dux = (u(j,i)-u(j,i-1))/dx;
%     dvy = (v(j,i)-v(j-1,i))/dy;
%     D(j,i) = dux+dvy;
    
end

