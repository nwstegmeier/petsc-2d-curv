function compute_face_vel()

    global uxN vyN u v nx ny

    for j=2:ny+2
        for i=2:nx+2
            uxN(j,i,1)=uxN(j,i,2);
            vyN(j,i,1)=vyN(j,i,2);
            uxN(j,i,2)=0.5*(u(j,i)+u(j,i-1));
            vyN(j,i,2)=0.5*(v(j,i)+v(j-1,i));
        end
    end
    
end

