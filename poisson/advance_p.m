function advance_p()

    global A b x y u v uxN vyN dx dy dt nx ny D D2x D2y Re pex time
    
    A = zeros(nx*ny,nx*ny);
    b = zeros(nx*ny,1);
    
    for j=2:ny+1
        for i=2:nx+1
            
            ap  = -2*(dx^2)-2*(dy^2);
            aex = dy^2;
            awx = dy^2;
            aey = dx^2;
            awy = dx^2;
            
            uu2x = ( (u(j,i-1)^2)-2*(u(j,i)^2)+(u(j,i+1)^2) )/(dx^2);
            vv2y = ( (v(j-1,i)^2)-2*(v(j,i)^2)+(v(j+1,i)^2) )/(dy^2);
            uvxy = (  uxN(j+1,i+1,2)*vyN(j+1,i+1,2) ...
                    - uxN(j+1,i  ,2)*vyN(j+1,i  ,2) ...
                    - uxN(j  ,i+1,2)*vyN(j  ,i+1,2) ...
                    + uxN(j  ,i  ,2)*vyN(j  ,i  ,2) )/(dx*dy);
            rhs  = (dx^2)*(dy^2)*( D(j,i)/dt - uu2x - vv2y -2*uvxy + (1/Re)*( D2x(j,i) + D2y(j,i) ) );
            
            % matrix set up
            m = (j-2)*nx + (i-1);  
            b(m) = rhs;
            idx = [m m+1 m-1 m+nx m-nx];
            idx = neumannbc(idx,i,j);
            A(idx(1),idx(1)) = A(idx(1),idx(1)) + ap;
            A(idx(1),idx(2)) = A(idx(1),idx(2)) + aex;
            A(idx(1),idx(3)) = A(idx(1),idx(3)) + awx;
            A(idx(1),idx(4)) = A(idx(1),idx(4)) + aey;
            A(idx(1),idx(5)) = A(idx(1),idx(5)) + awy; 
            
        end
    end
    
    sol = A\b;
    
    for j=1:ny
        for i=1:nx
            m = (j-1)*nx+i;
            p(j+1,i+1) = sol(m);
        end
    end
    
    % outer boundary conditions
    p(:,1)=p(:,2);
    p(1,:)=p(2,:);
    p(:,ny+2)=p(:,ny+1);
    p(nx+2,:)=p(nx+1,:);
    
    time = time+dt;
    for jj=0:ny+1
        for ii=0:nx+1
            i=ii+1; j=jj+1;     
            pex(j,i)=-0.25*(cos(2*x(i))+cos(2*y(j)))*exp((-4/Re)*time);
        end
    end
    
    pex(:,1)=pex(:,2);
    pex(1,:)=pex(2,:);
    pex(:,ny+2)=pex(:,ny+1);
    pex(nx+2,:)=pex(nx+1,:);

end

