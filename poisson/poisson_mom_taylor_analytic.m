% solve momentum x-dir
clear all;
close all;
clc;

% declaration
global A b uxN vyN d gx gy u v p ...
    nx ny nt x0 y0 t0 lx ly lt dt dx dy Re ...
    D;

% grid creation
nx = 20;    x0 = 0.0;   lx =  2*pi;    dx = lx/double(nx);
ny = 20;    y0 = 0.0;   ly =  2*pi;    dy = ly/double(ny);
nt = 1000;  t0 = 0.0;   lt =  10.0;    dt = lt/double(nt);
x = linspace(x0-dx/2.0,lx+dx/2.0,nx+2);
y = linspace(y0-dy/2.0,ly+dy/2.0,ny+2);
[X Y] = meshgrid(x,y);

Re = 10;
time = t0;

% preallocation
[d, gx, gy, u, v, p, f] = deal(zeros(ny+2,nx+2));
[uxN,vyN] = deal(zeros(ny+2,nx+2,2));
A = zeros(nx*ny,nx*ny);
b = zeros(nx*ny,1);
sol = zeros(nx*ny,1);

% initial conditions
for jj=0:ny+1
    for ii=0:nx+1
        i=ii+1; j=jj+1;
        d(j,i)=1.0;
        u(j,i)=-cos(x(i))*sin(y(j));
        v(j,i)=sin(x(i))*cos(y(j));
        gx(j,i)=d(j,i)*u(j,i);
        gy(j,i)=d(j,i)*v(j,i);     
        p(j,i)=-0.25*(cos(2*x(i))+cos(2*y(j)))*exp(-4*(1/Re)*(dt+3/2));
    end
end

compute_face_vel();

for l=1:nt
    
    advance_gx();
    advance_gy();
%     solve_p();
    compute_face_vel();
    g = sqrt(gx.^2+gy.^2);
    
    time = time+dt;
    
    if (mod(l,100)==0)
        figure(1)
        subplot(2,2,1)
        pcolor(Y,X,g); shading interp; colorbar; caxis([-1 1]);
        subplot(2,2,2)
        pcolor(Y,X,p); shading interp; colorbar; caxis([-0.5 0.5]);
        subplot(2,2,3)
        pcolor(Y,X,u); shading interp; colorbar; caxis([-1 1]);
%         plot(x,u(2,:))
%         axis([x0 lx -1 1])
        subplot(2,2,4)
        pcolor(Y,X,v); shading interp; colorbar; caxis([-1 1]);
%         plot(y,v(2,:))
%         axis([y0 ly -1 1])
        M(l)=getframe;   
        l
    end
    
    for jj=0:ny+1
        for ii=0:nx+1
            i=ii+1; j=jj+1;     
            p(j,i)=-0.25*(cos(2*x(i))+cos(2*y(j)))*exp((-4/Re)*time);
        end
    end
    
end



