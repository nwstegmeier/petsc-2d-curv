% poisson eqn solver
clear all; close all; clc;

nx = 4;
ny = 4;
x0 = 0.0;
y0 = 0.0;
lx = 2*pi;
ly = 2*pi;
dx = lx/double(nx);
dy = ly/double(ny);
x = linspace(x0-dx/2.0,lx+dx/2.0,nx+2);
y = linspace(y0-dy/2.0,ly+dy/2.0,ny+2);
[u,f] = deal(zeros(nx+2,ny+2));
A = zeros(nx*ny,nx*ny);
b = zeros(nx*ny,1);
sol = zeros(nx*ny);

TE = 100*ones(nx,1);
TW = 500*ones(nx,1);
TN = 500*ones(ny,1);
TS = 100*ones(ny,1);

% set initial conditions
for ii = 0:nx+1
    for jj = 0:ny+1
        i=ii+1; j=jj+1;
        u(i,j) = x(i)+2*y(j);
        f(i,j) = 0.0;
    end
end

% boundary conditions
% i=1 --> 
% A(1,1)=-4;
% A(1,2)=

for j = 1:ny
    for i = 1:nx
        m = (j-1)*nx + i;
%         sprintf('%i %i %i \n',ii,jj,m)
        ap = -2*dx*dx-2*dy*dy;
        aex = dy*dy;
        awx = dy*dy;
        aey = dx*dx;
        awy = dx*dx;
        idx = [m m+1 m-1 m+nx m-nx];
        
        b(m) = f(i,j); 
        
        if i==1
            idx(3)=m+1;
            b(m) = b(m)+-dy*dy*TW(j);
            awx = 0.0;
        end
        if j==1
            idx(5)=m+nx;
            b(m) = b(m)+-dx*dx*TS(i);
            awy = 0.0;
        end
        if i==nx
            idx(2)=m-1;
            b(m) = b(m)+-dy*dy*TE(j);
            aex = 0.0;
        end
        if j==ny
            idx(4)=m-nx;
            b(m) = b(m)+-dx*dx*TN(i);
            aey = 0.0;
        end
        
        A(idx(1),idx(1)) = A(idx(1),idx(1)) + ap;
        A(idx(1),idx(2)) = A(idx(1),idx(2)) + aex;
        A(idx(1),idx(3)) = A(idx(1),idx(3)) + awx;
        A(idx(1),idx(4)) = A(idx(1),idx(4)) + aey;
        A(idx(1),idx(5)) = A(idx(1),idx(5)) + awy;       
    
    end
end

sol = inv(A)*b;

for jj = 1:ny
    for ii=1:nx
        m = (jj-1)*nx+ii;
        u(jj,ii)=sol(m);
    end
end

% u = u(1:nx,1:ny);

pcolor(x(2:nx+1),y(2:ny+1),u(1:nx,1:ny));
colorbar
shading interp
