function advance_gy()

    global A b gx gy u v uxN vyN d p nx ny dt dx dy Re

    A = zeros(nx*ny,nx*ny);
    b = zeros(nx*ny,1);
    
    for j=2:ny+1
        for i=2:nx+1
            
            ap  =  1.0 + 0.25*(dt/dx)*(0.5*(uxN(j,i+1,2)+uxN(j,i+1,1))-0.5*(uxN(j,i,2)+uxN(j,i,1))) ...
                       + 0.25*(dt/dy)*(0.5*(vyN(j+1,i,2)+vyN(j+1,i,1))-0.5*(vyN(j,i,2)+vyN(j,i,1)));
            aex =  0.25*(dt/dx)*(0.5*(uxN(j  ,i+1,2)+uxN(j  ,i+1,1)));
            awx = -0.25*(dt/dx)*(0.5*(uxN(j  ,i  ,2)+uxN(j  ,i  ,1)));
            aey =  0.25*(dt/dy)*(0.5*(vyN(j+1,i  ,2)+vyN(j+1,i  ,1)));
            awy = -0.25*(dt/dy)*(0.5*(vyN(j  ,i  ,2)+vyN(j  ,i  ,1)));
            
            bp  =  1.0 - 0.25*(dt/dx)*(0.5*(uxN(j,i+1,2)+uxN(j,i+1,1))-0.5*(uxN(j,i,2)+uxN(j,i,1))) ...
                       - 0.25*(dt/dy)*(0.5*(vyN(j+1,i,2)+vyN(j+1,i,1))-0.5*(vyN(j,i,2)+vyN(j,i,1)));
            bex = -0.25*(dt/dx)*(0.5*(uxN(j  ,i+1,2)+uxN(j  ,i+1,1)));
            bwx =  0.25*(dt/dx)*(0.5*(uxN(j  ,i  ,2)+uxN(j  ,i  ,1)));
            bey = -0.25*(dt/dy)*(0.5*(vyN(j+1,i  ,2)+vyN(j+1,i  ,1)));
            bwy =  0.25*(dt/dy)*(0.5*(vyN(j  ,i  ,2)+vyN(j  ,i  ,1)));
            
            rhs =   bp*gy(j,i) ...
                  + bex*gy(j,i+1) + bwx*gy(j,i-1) ...
                  + bey*gy(j+1,i) + bwy*gy(j-1,i) ...
                  - 0.5*(dt/dy)*( p(j+1,i)-p(j-1,i) ) ...
                  + (dt/Re)*( v(j,i-1)-2*v(j,i)+v(j,i+1) )/(dx*dx) ...
                  + (dt/Re)*( v(j-1,i)-2*v(j,i)+v(j+1,i) )/(dy*dy);
            
            set_matrix_rhs(i,j,ap,aex,awx,aey,awy,rhs,0);
                        
        end
    end
    
    sol = A\b;
     
    for j=1:ny
        for i=1:nx
            m = (j-1)*nx+i;
            gy(j+1,i+1) = sol(m);
        end
    end
     
    gy = update_outer_bc(gy); 
    
    v(:,:) = gy(:,:)./d(:,:);

end

