function [ idxs ] = perbc( idx,i,j )

    global nx ny;

    ii=i-1;
    jj=j-1;

    if ii==1
        idx(3)= (jj-1)*nx+nx;
    end
    if jj==1
        idx(5)= (ny-1)*nx+ii;
    end
    if ii==nx
        idx(2)= (jj-1)*nx+1;
    end
    if jj==ny
        idx(4)= (1-1)*nx+ii;
    end
    
    idxs = idx;

end

