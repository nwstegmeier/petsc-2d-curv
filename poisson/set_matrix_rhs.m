function set_matrix_rhs( i,j,ap,aex,awx,aey,awy,rhs,BC)

        global A b nx ny;

        % matrix indices
        m = (j-2)*nx + (i-1);  
        idx = [m m+1 m-1 m+nx m-nx];

        b(m) = rhs; 

        if(BC==0)
            idx = perbc(idx,i,j);
        end
        if(BC==1)
            idx = neumannbc(idx,i,j);
        end

        A(idx(1),idx(1)) = A(idx(1),idx(1)) + ap;
        A(idx(1),idx(2)) = A(idx(1),idx(2)) + aex;
        A(idx(1),idx(3)) = A(idx(1),idx(3)) + awx;
        A(idx(1),idx(4)) = A(idx(1),idx(4)) + aey;
        A(idx(1),idx(5)) = A(idx(1),idx(5)) + awy; 

end

