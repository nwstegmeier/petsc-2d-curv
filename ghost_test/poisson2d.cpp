#include <petsc.h>
// Nick Stegmeier -- 11/20/17
// poisson2d_dmda.cpp

// Description:
//    This code solves a 2D poisson problem. The primary purpose is to test
//    the implementation of ghost points using MPI with PETSc DMDA.

// variable declaration
const PetscMPIInt       *neighbors;
PetscMPIInt             rank,size,ierr;
PetscMPIInt             nbrleft,nbrright,nbrbottom,nbrtop;  // neighboring processors
MPI_Datatype            xface,yface;
MPI_Status              status;
PetscInt 		            xs,xw,ys,yw;
PetscInt                firstx,firsty,lastx,lasty;
PetscInt                global_firstx,global_firsty,global_lastx,global_lasty;
PetscInt                i,j,k,t=2,zone;
PetscInt 			          dof=1,sw=1,ione=1;
DMBoundaryType 		      bx=DM_BOUNDARY_GHOSTED; // ghosted works
DMBoundaryType 		      by=DM_BOUNDARY_GHOSTED;
DMDAStencilType 	      stype=DMDA_STENCIL_BOX;
DM                      da;
Vec                     bv,solv;
KSP                     ksp;
Mat                     A;
PC 				              pcksp;
PetscScalar             **b,**sol;
PetscReal               global_total_diff;
FILE *file1 = NULL;
char filename1[PETSC_MAX_PATH_LEN];

// parameters
const PetscInt  nx=20,ny=20;
const PetscInt  max_it=10000;
const PetscReal tol=10E-6;
PetscInt        n=0,num_save=0,save_interval=500;
PetscReal       lx0=0.0,ly0=0.0,lx=1.0,ly=1.0;
PetscReal       dx,dy,dt;
PetscReal       BC_S=200,BC_N=800,BC_E=500,BC_W=200;

double *psiMemLoc, *xMemLoc, *yMemLoc;
double ***psi,**x,**y;

// function declaration
extern void update_ghost(double ***a, int k);
extern void init_cond();
extern void outer_bc();
extern void save_data(int n,int rank);
extern void solve_poisson();
extern void compute_diff();

// main program
int main(int argc,char **argv){
  	
	PetscInitialize(&argc,&argv,(char*)0,NULL);
	MPI_Comm_size(PETSC_COMM_WORLD,&size);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  
  PetscSNPrintf(filename1,sizeof(filename1),"logfile%i.dat",rank);
  PetscFOpen(PETSC_COMM_SELF,filename1,"w",&file1);
  PetscFPrintf(PETSC_COMM_SELF,file1,"my rank: %i \n",rank);	
  
  dx = (lx-lx0)/(double)nx;
	dy = (ly-ly0)/(double)ny;
  dt =(1.0/16.0)*dx*dx;
  
  // use DMDA to decompose+distribute domain
  DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,ione,sw,NULL,NULL,&da);
  DMSetFromOptions(da);
  DMSetUp(da);
  DMDAGetCorners(da,&xs,&ys,0,&xw,&yw,0);
  DMView(da,PETSC_VIEWER_STDOUT_WORLD);
  
  DMCreateGlobalVector(da,&bv);
  DMCreateGlobalVector(da,&solv);	
  VecSetUp(bv); 
  VecSetUp(solv);
  VecSetFromOptions(bv); 
  VecSetFromOptions(solv); 
  
  DMCreateMatrix(da,&A);
	MatSetFromOptions(A);
	MatSetUp(A);

  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetType(ksp,KSPGMRES);
  KSPGetPC(ksp,&pcksp);
  PCSetType(pcksp,PCJACOBI);
  KSPSetPC(ksp,pcksp);
  KSPSetFromOptions(ksp);
  KSPSetOperators(ksp,A,A);
  KSPSetUp(ksp);
  
  // define the "interior" domain
  firstx=1; 
  firsty=1;
  lastx=xw;
  lasty=yw;
  // define location in global domain
  global_firstx = xs+1;
  global_lastx  = xs+xw;
  global_firsty = ys+1;
  global_lasty  = ys+yw;
  
  xMemLoc   = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  yMemLoc   = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  x   = (double **) malloc(sizeof(double *) * (lasty-firsty+3));
  y   = (double **) malloc(sizeof(double *) * (lasty-firsty+3));
  for(j=firsty-1; j<=lasty+1; j++){
    x[j]   = xMemLoc + (lastx-firstx+3)*j;
    y[j]   = yMemLoc + (lastx-firstx+3)*j;
  }
  
  psiMemLoc = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  psi = new double**[lasty-firsty+3];
  for(j=firsty-1; j<=lasty+1; j++){
    psi[j] = new double*[lastx-firstx+3];
    for (i=firstx-1; i<=lastx+1; i++){
      psi[j][i] = psiMemLoc + j*(lastx-firstx+3)*3 + i*3;// + i*3;
    }
  }

  // determine neighboring processors
  DMDAGetNeighbors(da,&neighbors);
  nbrleft		= neighbors[3];
  nbrright	= neighbors[5];
  nbrbottom	= neighbors[1];
  nbrtop		= neighbors[7];
  if(nbrleft<0){nbrleft=MPI_PROC_NULL;}
  if(nbrright<0){nbrright=MPI_PROC_NULL;}
  if(nbrbottom<0){nbrbottom=MPI_PROC_NULL;}
  if(nbrtop<0){nbrtop=MPI_PROC_NULL;}
  PetscFPrintf(PETSC_COMM_SELF,file1,"nbrleft %i, nbrright %i, nbrbottom %i, nbrtop %i \n",nbrleft,nbrright,nbrbottom,nbrtop);
  PetscFPrintf(PETSC_COMM_SELF,file1,"firstx %i, lastx %i, firsty %i, lasty %i \n",firstx,lastx,firsty,lasty);
  PetscFPrintf(PETSC_COMM_SELF,file1,"global_firstx %i, global_lastx %i, global_firsty %i, global_lasty %i \n",global_firstx,global_lastx,global_firsty,global_lasty);
  PetscFPrintf(PETSC_COMM_SELF,file1,"xs %i, xs+xw %i, ys %i, ys+yw %i \n",xs,xs+xw,ys,ys+yw);
    
  // create custom MPI data-type for exchanging xface boundaries
  MPI_Type_vector(lasty-firsty+3,1,(lastx-firstx+3)*3,MPI_DOUBLE_PRECISION,&xface);
  MPI_Type_commit(&xface);
  MPI_Type_vector((lastx-firstx+3),1,3,MPI_DOUBLE_PRECISION,&yface);
  MPI_Type_commit(&yface);
  
  // pre-timestepping maintenance
  init_cond();
  outer_bc();
  save_data(n,rank);
  update_ghost(psi,t);
 
  for (n=1; n<=max_it; n++){
    
   solve_poisson();
   update_ghost(psi,t);
   compute_diff();

   PetscPrintf(PETSC_COMM_SELF,"Iter %i. Diff %e\n",n,global_total_diff);

   if(n%save_interval==0){
   save_data(n,rank);    
   PetscPrintf(PETSC_COMM_SELF,"Save complete. \n");
   }
   
  }
  
  PetscFPrintf(PETSC_COMM_SELF,file1,"Simulation complete. \n");
  fclose(file1);
  file1 = NULL; 
  PetscFinalize(); 
  return(ierr);
		
}

void solve_poisson(){
  PetscReal	  ap,aex,awx,aey,awy,rhs,vals[5];
  MatStencil 	row,col[5];
  PetscInt    ii,jj;
  
	DMDAVecGetArray(da,bv,&b);
	PetscMemzero(col,5*sizeof(MatStencil));
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs-1;
    jj=j+ys-1;
    ap=(1.0+2.0*dt/(dx*dx)+2.0*dt/(dy*dy));
    awx=-dt/(dx*dx);
    aex=-dt/(dx*dx);
    awy=-dt/(dy*dy);
    aey=-dt/(dy*dy);
    rhs=psi[j][i][t];
    //rhs=psi[j][i]*(1.0-2.0*dt/(dx*dx)-2.0*dt/(dy*dy))
        //+dt*psi[j][i-1]/(dx*dx)+dt*psi[j][i+1]/(dx*dx)+dt*psi[j-1][i]/(dy*dy)+dt*psi[j+1][i]/(dy*dy);
    
    vals[0] = ap;
    vals[1] = awx;
    vals[2] = aex;
    vals[3] = awy;
    vals[4] = aey;
    b[jj][ii] = rhs;
    
    //PetscPrintf(PETSC_COMM_SELF,"%i %i \n",ii,jj);
    
     if(ii==0){
       //vals[0]=vals[0]+awx;
       vals[1]=0.0;
       b[jj][ii] = b[jj][ii]-awx*BC_W;
     }
     if(ii==nx-1){
       //vals[0]=vals[0]+aex;
       vals[2]=0.0;
       b[jj][ii] = b[jj][ii]-aex*BC_E;
     }
     if(jj==0){
       //vals[0]=vals[0]+awy;
       vals[3]=0.0;
       b[jj][ii] = b[jj][ii]-awy*BC_S;
     }
     if(jj==ny-1){
       //vals[0]=vals[0]+aey;
       vals[4]=0.0;
       b[jj][ii] = b[jj][ii]-aey*BC_N;;
     }
    
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = ii  ;    row.j = jj  ; // matrix row
    col[0].i = ii  ; col[0].j = jj  ; // ap
    col[1].i = ii-1; col[1].j = jj  ; // awx
    col[2].i = ii+1; col[2].j = jj  ; // aex
    col[3].i = ii  ; col[3].j = jj-1; // awy 
    col[4].i = ii  ; col[4].j = jj+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
        
  }
  }
  DMDAVecRestoreArray(da,bv,&b);
  
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
  
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  
  KSPSolve(ksp,bv,solv);
  //VecView(solv,PETSC_VIEWER_STDOUT_WORLD);
  
  DMDAVecGetArray(da,solv,&sol);
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs;
    jj=j+ys;
    psi[j][i][t-1]=psi[j][i][t];
    psi[j][i][t]=sol[jj-1][ii-1];
    //PetscPrintf(PETSC_COMM_SELF,"psi[%i][%i] %f. \n",j,i,psi[j][i]);  
  }
  }
  DMDAVecRestoreArray(da,solv,&sol);
  
  MatZeroEntries(A);
  
}

void compute_diff(){
  PetscReal local_total_diff;
  PetscInt ii,jj;
  
  for (j = firsty; j <= lasty; j++){
  for (i = firstx; i <= lastx; i++){
    local_total_diff= PetscAbsReal(psi[j][i][t-1]-psi[j][i][t]);
  }
  }
  MPI_Barrier(PETSC_COMM_WORLD);
  MPI_Reduce(&local_total_diff,&global_total_diff,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,PETSC_COMM_WORLD);
  
}

// this function updates the ghost points
void update_ghost(double ***a, int t_idx){  

  // exchange ghost points on y faces
    // send last row of "a" to "nbrtop" + receive last row of from "nbrbottom"
    MPI_Sendrecv(&a[lasty   ][firstx-1][t_idx],1,yface,nbrtop   ,0,  \
                 &a[firsty-1][firstx-1][t_idx],1,yface,nbrbottom,0,  \
                 PETSC_COMM_WORLD,&status);
    //MPI_Barrier(PETSC_COMM_WORLD);
    //PetscFPrintf(PETSC_COMM_SELF,file1,"yface send lasty successful. \n");
    // send first row of "a" to "nbrbottom" + receive first row of "a" from "nbrbottom"
    MPI_Sendrecv(&a[firsty  ][firstx-1][t_idx],1,yface,nbrbottom,1,  \
                 &a[lasty+1 ][firstx-1][t_idx],1,yface,nbrtop   ,1,  \
                 PETSC_COMM_WORLD,&status);
    //MPI_Barrier(PETSC_COMM_WORLD);
    //PetscFPrintf(PETSC_COMM_SELF,file1,"yface send firsty successful. \n");
  
  // exchange ghost points on x faces
    // send last column of "a" to "nbrright" + receive last column of from "nbrleft"
    MPI_Sendrecv(&a[firsty-1][lastx   ][t_idx],1,xface,nbrright,0,  \
                 &a[firsty-1][firstx-1][t_idx],1,xface,nbrleft ,0,  \
                 PETSC_COMM_WORLD,&status);
    //MPI_Barrier(PETSC_COMM_WORLD);
    //PetscFPrintf(PETSC_COMM_SELF,file1,"xface send lastx successful. \n");
    // send first column of "a" to "nbrleft" + receive first column of from "nbrright"
    MPI_Sendrecv(&a[firsty-1][firstx  ][t_idx],1,xface,nbrleft ,1,  \
                 &a[firsty-1][lastx+1 ][t_idx],1,xface,nbrright,1,  \
                 PETSC_COMM_WORLD,&status);
   
    MPI_Barrier(PETSC_COMM_WORLD);
    //PetscFPrintf(PETSC_COMM_SELF,file1,"xface send firstx successful. \n");
               
}

void init_cond(){ 
  for(k=0; k<=2; k++){
    for(j=firsty-1; j<=lasty+1; j++){
      for(i=firstx-1; i<=lastx+1; i++){
        x[j][i]= -dx/2.0 + (float)(i+xs)*dx;
        y[j][i]= -dy/2.0 + (float)(j+ys)*dy;
        psi[j][i][k] = 300.0;
        //PetscPrintf(PETSC_COMM_SELF,"x[%i][%i]=%f y[%i][%i]=%f psi[%i][%i][%i]=%f \n",j,i,x[j][i],j,i,y[j][i],j,i,k,psi[j][i][k]);
      }
    }
  }
  PetscFPrintf(PETSC_COMM_SELF,file1,"init_cond successful. \n");
}

void outer_bc(){
  if(global_firstx==1){
    for(j=firsty-1; j<=lasty+1; j++){
      psi[j][0][t]=BC_W;
    }
  }
  if(global_lastx==nx){
    for(j=firsty-1; j<=lasty+1; j++){
      psi[j][lastx+1][t]=BC_E;
    }
  }
  if(global_firsty==1){
    for(i=firstx-1; i<=lastx+1; i++){
      psi[0][i][t]=BC_S;
    }
  }
  if(global_lasty==ny)
  {
    for(i=firstx-1; i<=lastx+1; i++){
      psi[lasty+1][i][t]=BC_N;
    }
  }
  if(global_firsty==1 && global_firstx==1){psi[0      ][0       ][t]  =0.5*(BC_S+BC_W);}
  if(global_lasty==ny && global_firstx==1){psi[lasty+1][0       ][t]  =0.5*(BC_S+BC_W);}
  if(global_lasty==ny && global_lastx==nx){psi[lasty+1][lastx+1 ][t]  =0.5*(BC_N+BC_E);}
  if(global_firsty==1 && global_lastx==nx){psi[0      ][lastx+1 ][t]  =0.5*(BC_S+BC_E);}
  
}

void save_data(int n,int rank){
  FILE *file = NULL;
  char filename[PETSC_MAX_PATH_LEN];
  PetscInt i,j,k,zone;  
  PetscReal zp;
  
  if(num_save==0){
    zone=0;
  } else {
    zone=n/save_interval;
  }
  PetscSNPrintf(filename,sizeof(filename),"data/YY%04i.%04i.dat",rank,num_save);
  PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
  PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,psi\n");	
  PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\",I=%i J=%i K=4 F=POINT\n",zone,xw+2,yw+2);
  for (k = 0; k < 4; k++){
    for (j = firsty-1; j <= lasty+1; j++){
      for (i = firstx-1; i <= lastx+1; i++){
        zp=k*0.5+0.5/2.0;		
        PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e \n",x[j][i],y[j][i],zp,psi[j][i][t]);
      }		
    }
  }

  fclose(file);
  file = NULL;
  num_save=num_save+1;
  
}
