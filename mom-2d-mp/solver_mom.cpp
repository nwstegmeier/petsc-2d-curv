#include "driver.h"
#include "xml.h"
#include <petsc.h>

void advance_gx(){
  PetscInt	  i,j,ii,jj;
  PetscReal	  apf,apbf,aexf,awxf,aeyf,awyf,rhsf,vals[5];
  PetscReal   bpf,brsf,bexf,bwxf,beyf,bwyf;
  MatStencil 	row,col[5];
  
  store_current_ts(gxs,gx);
  
  DMDAVecGetArray(da,bv,&b);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs-1;
    jj=j+ys-1;
    
    apf   =  1.0+ 0.25*dt*( 0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1])-0.5*(uxN[j][i][t]+uxN[j][i][t-1]) )/dx 
                + 0.25*dt*( 0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1])-0.5*(vyN[j][i][t]+vyN[j][i][t-1]) )/dy;
    aexf  =  0.25*dt*(0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]))/dx;
    awxf  = -0.25*dt*(0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]))/dx;
    aeyf  =  0.25*dt*(0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]))/dy;
    awyf  = -0.25*dt*(0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]))/dy;

    bpf  = 1.0 - 0.25*dt*( 0.0 
               + (0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]) - 0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]) )/dx  
               + (0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]) - 0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]) )/dy );
          
    bexf = -0.25*dt*( 0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]) )/dx;
    bwxf =  0.25*dt*( 0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]) )/dx; 
    beyf = -0.25*dt*( 0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]) )/dy;
    bwyf =  0.25*dt*( 0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]) )/dy;
    
    brsf = 0.0;
    apbf = 0.0;
    
    rhsf  = bpf*gx[j][i][t-1]  
           + bexf*gx[j  ][i+1][t-1] + bwxf*gx[j  ][i-1][t-1]
           + beyf*gx[j+1][i  ][t-1] + bwyf*gx[j-1][i  ][t-1];
           
    update_bc(i,j,gx,gx_in,gx_out,gx_bot,gx_top,
               apf,rhsf,bpf,apbf,brsf,
               aexf,aeyf,awxf,awyf,
               bexf,beyf,bwxf,bwyf,
               BCxL,BCyL,BCxR,BCyR);
          
    vals[0] = apf+apbf;
    vals[1] = awxf;
    vals[2] = aexf;
    vals[3] = awyf;
    vals[4] = aeyf;
    b[jj][ii] = rhsf;
  
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = ii  ;    row.j = jj  ; // matrix row
    col[0].i = ii  ; col[0].j = jj  ; // ap
    col[1].i = ii-1; col[1].j = jj  ; // awx
    col[2].i = ii+1; col[2].j = jj  ; // aex
    col[3].i = ii  ; col[3].j = jj-1; // awy 
    col[4].i = ii  ; col[4].j = jj+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
    
  }
  }
  
  DMDAVecRestoreArray(da,bv,&b);
  
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
 
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  
  KSPSolve(ksp,bv,solv);
  MatZeroEntries(A);
  
  DMDAVecGetArray(da,solv,&sol);
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs;
    jj=j+ys;
    gx[j][i][t]=sol[jj-1][ii-1];
    //PetscPrintf(PETSC_COMM_SELF,"gx[%i][%i] %20.16f \n",i,j,gx[j][i][t]);  
  }
  }
  DMDAVecRestoreArray(da,solv,&sol);
    
  update_ghost_3D(gx,t);
  update_outer_bc(gx,gx_in,gx_out,gx_bot,gx_top,BCxL,BCyL,BCxR,BCyR);
  
}

void advance_gy(){
  PetscInt	  i,j,ii,jj;
  PetscReal	  apf,apbf,aexf,awxf,aeyf,awyf,rhsf,vals[5];
  PetscReal   bpf,brsf,bexf,bwxf,beyf,bwyf;
  MatStencil 	row,col[5];
  
  store_current_ts(gys,gy);
  
  DMDAVecGetArray(da,bv,&b);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs-1;
    jj=j+ys-1;
    
    apf   =  1.0+ 0.25*dt*( 0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1])-0.5*(uxN[j][i][t]+uxN[j][i][t-1]) )/dx 
                + 0.25*dt*( 0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1])-0.5*(vyN[j][i][t]+vyN[j][i][t-1]) )/dy;
    aexf  =  0.25*dt*(0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]))/dx;
    awxf  = -0.25*dt*(0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]))/dx;
    aeyf  =  0.25*dt*(0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]))/dy;
    awyf  = -0.25*dt*(0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]))/dy;

    bpf  = 1.0 - 0.25*dt*( 0.0 
               + (0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]) - 0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]) )/dx  
               + (0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]) - 0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]) )/dy );
          
    bexf = -0.25*dt*( 0.5*(uxN[j  ][i+1][t]+uxN[j  ][i+1][t-1]) )/dx;
    bwxf =  0.25*dt*( 0.5*(uxN[j  ][i  ][t]+uxN[j  ][i  ][t-1]) )/dx; 
    beyf = -0.25*dt*( 0.5*(vyN[j+1][i  ][t]+vyN[j+1][i  ][t-1]) )/dy;
    bwyf =  0.25*dt*( 0.5*(vyN[j  ][i  ][t]+vyN[j  ][i  ][t-1]) )/dy;
    
    brsf = 0.0;
    apbf = 0.0;
    
    rhsf  = bpf*gy[j][i][t-1]  
           + bexf*gy[j  ][i+1][t-1] + bwxf*gy[j  ][i-1][t-1]
           + beyf*gy[j+1][i  ][t-1] + bwyf*gy[j-1][i  ][t-1];
           
    update_bc(i,j,gy,gy_in,gy_out,gy_bot,gy_top,
               apf,rhsf,bpf,apbf,brsf,
               aexf,aeyf,awxf,awyf,
               bexf,beyf,bwxf,bwyf,
               BCxL,BCyL,BCxR,BCyR);
          
    vals[0] = apf+apbf;
    vals[1] = awxf;
    vals[2] = aexf;
    vals[3] = awyf;
    vals[4] = aeyf;
    b[jj][ii] = rhsf;
  
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = ii  ;    row.j = jj  ; // matrix row
    col[0].i = ii  ; col[0].j = jj  ; // ap
    col[1].i = ii-1; col[1].j = jj  ; // awx
    col[2].i = ii+1; col[2].j = jj  ; // aex
    col[3].i = ii  ; col[3].j = jj-1; // awy 
    col[4].i = ii  ; col[4].j = jj+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
    
  }
  }
  
  DMDAVecRestoreArray(da,bv,&b);
  
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
 
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  
  KSPSolve(ksp,bv,solv);
  MatZeroEntries(A);
  
  DMDAVecGetArray(da,solv,&sol);
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs;
    jj=j+ys;
    gy[j][i][t]=sol[jj-1][ii-1];
    //PetscPrintf(PETSC_COMM_SELF,"gy[%i][%i] %20.16f \n",i,j,gy[j][i][t]);  
  }
  }
  DMDAVecRestoreArray(da,solv,&sol);
    
  update_ghost_3D(gy,t);
  update_outer_bc(gy,gy_in,gy_out,gy_bot,gy_top,BCxL,BCyL,BCxR,BCyR);
  
}

