#include "driver.h"
#include <petsc.h>

void advance_continuity(){
  PetscInt	  i,j,ii,jj;
  PetscReal	  apf,apbf,aexf,awxf,aeyf,awyf,rhsf,vals[5];
  PetscReal   bpf,brsf,bexf,bwxf,beyf,bwyf;
  MatStencil 	row,col[5];
  
  store_current_ts(rhos,rho);
  
  DMDAVecGetArray(da,bv,&b);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs-1;
    jj=j+ys-1;
    
    apf  = 1.0 + 0.25*dt*(0.0 + ( uxN[j  ][i+1][t] - uxN[j][i][t] )/dx
                              + ( vyN[j+1][i  ][t] - vyN[j][i][t] )/dy );
                            
    aexf =  0.25*dt*uxN[j  ][i+1][t]/dx;
    awxf = -0.25*dt*uxN[j  ][i  ][t]/dx;
    aeyf =  0.25*dt*vyN[j+1][i  ][t]/dy;
    awyf = -0.25*dt*vyN[j  ][i  ][t]/dy;
    
    bpf  = 1.0 - 0.25*dt*( 0.0 + ( uxN[j  ][i+1][t] - uxN[j][i][t] )/dx 
                               + ( vyN[j+1][i  ][t] - vyN[j][i][t] )/dy );
                             
    bexf = -0.25*dt*uxN[j  ][i+1][t]/dx;
    bwxf =  0.25*dt*uxN[j  ][i  ][t]/dx;
    beyf = -0.25*dt*vyN[j+1][i  ][t]/dy;
    bwyf =  0.25*dt*vyN[j  ][i  ][t]/dy;
    
    brsf = 0.0;
    apbf = 0.0;
    
    rhsf = bpf*rho[j][i][t-1]
            + bexf*rho[j  ][i+1][t-1] + bwxf*rho[j  ][i-1][t-1]
            + beyf*rho[j+1][i  ][t-1] + bwyf*rho[j-1][i  ][t-1];

    update_bc(i,j,rho,rho_in,rho_out,rho_bot,rho_top,
               apf,rhsf,bpf,apbf,brsf,
               aexf,aeyf,awxf,awyf,
               bexf,beyf,bwxf,bwyf,
               BCxL,BCyL,BCxR,BCyR);
               
    //PetscPrintf(PETSC_COMM_SELF,"[%i][%i] %14.12e %14.12e %14.12e %14.12e \n",i,j,apf+apbf,awxf,aeyf,rightf); 
    
    vals[0] = apf+apbf;
    vals[1] = awxf;
    vals[2] = aexf;
    vals[3] = awyf;
    vals[4] = aeyf;
    b[jj][ii] = rhsf;
  
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = ii  ;    row.j = jj  ; // matrix row
    col[0].i = ii  ; col[0].j = jj  ; // ap
    col[1].i = ii-1; col[1].j = jj  ; // awx
    col[2].i = ii+1; col[2].j = jj  ; // aex
    col[3].i = ii  ; col[3].j = jj-1; // awy 
    col[4].i = ii  ; col[4].j = jj+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
    
  }
  }
  
  DMDAVecRestoreArray(da,bv,&b);
  
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
 
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  
  KSPSolve(ksp,bv,solv);
  MatZeroEntries(A);
  
  DMDAVecGetArray(da,solv,&sol);
	for (j = firsty; j <= lasty; j++){
	for (i = firstx; i <= lastx; i++){
    ii=i+xs;
    jj=j+ys;
    rho[j][i][t]=sol[jj-1][ii-1];
    //PetscPrintf(PETSC_COMM_SELF,"rho[%i][%i] %20.16f \n",i,j,rho[j][i][t]);  
  }
  }
  DMDAVecRestoreArray(da,solv,&sol);
    
  update_ghost_3D(rho,t);
  update_outer_bc(rho,rho_in,rho_out,rho_bot,rho_top,BCxL,BCyL,BCxR,BCyR);
  
}