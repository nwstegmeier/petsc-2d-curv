#include <petsc.h>
#include "datastruct.h"
#include "driver.h"

// read in from test.sh file
PetscMPIInt   rank,size;
PetscInt		  n;
PetscInt		  nstart;
PetscInt 		  nstop;
PetscInt 		  save_interval;
PetscInt 		  num_save;
PetscInt		  yes_tecplot;
PetscInt 		  nx,ny;
PetscReal     dt;

// internally defined
PetscInt        xs,xw,ys,yw;
DM              da;
DMBoundaryType  bx,by;
Mat             A;
Vec             bv,solv;
KSP             ksp;
PC              pcksp;
PetscInt        firstx,firsty,lastx,lasty;
PetscInt        global_firstx,global_firsty;
PetscInt        global_lastx,global_lasty;
PetscReal       lx,ly,dx,dy;
PetscReal       sim_time;
PetscInt        t=2;
MPI_Datatype    xface3D,yface3D,xface2D,yface2D;
PetscMPIInt     nbrleft,nbrright,nbrbottom,nbrtop;
PetscReal       max_drho_global,max_dgx_global,max_dgy_global,max_dg_global;
PetscReal       ksp_tol,tolerance_drho,tolerance_dmom;
PetscInt        not_converged=1;

PetscInt     BCxL,BCyL,BCxR,BCyR;
 
double       *xMem,*yMem,*rhoMem,*gxMem,*gyMem,*uMem,*vMem,*uxNMem,*vyNMem;
double       *rhosMem,*gxsMem,*gysMem;
double       **x,**y,***rho,***gx,***gy,***u,***v,***uxN,***vyN;
double       **rhos,**gxs,**gys;
double       *rho_in,*rho_out,*rho_bot,*rho_top;
double       *gx_in,*gx_out,*gx_bot,*gx_top;
double       *gy_in,*gy_out,*gy_bot,*gy_top;
double       **b,**sol;

int main(int argc,char **argv){
	
	PetscInitialize(&argc,&argv,(char*)0,NULL);
	MPI_Comm_size(PETSC_COMM_WORLD,&size);
	MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
	
	PetscOptionsGetInt(NULL,NULL,"-nstart",&nstart,NULL);
	PetscOptionsGetInt(NULL,NULL,"-nstop",&nstop,NULL);
	PetscOptionsGetInt(NULL,NULL,"-saveint",&save_interval,NULL);
	PetscOptionsGetInt(NULL,NULL,"-num_save",&num_save,NULL);
	PetscOptionsGetInt(NULL,NULL,"-yes_tecplot",&yes_tecplot,NULL);
	PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);
	PetscOptionsGetInt(NULL,NULL,"-nx",&nx,NULL);
	PetscOptionsGetInt(NULL,NULL,"-ny",&ny,NULL);
  PetscOptionsGetReal(NULL,NULL,"-tolerance_drho",&tolerance_drho,NULL);
  PetscOptionsGetReal(NULL,NULL,"-tolerance_dmom",&tolerance_dmom,NULL);
  PetscOptionsGetReal(NULL,NULL,"-ksp_tol",&ksp_tol,NULL);
	
	set_up_Petsc();
  allocate_array_memory();
	solve_NSCH();
	
	PetscPrintf(PETSC_COMM_WORLD,"Simulation complete.\n\n"); 
  PetscFinalize();
		
}


