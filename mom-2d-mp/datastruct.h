#ifndef _DATASTRUCT_H
#define _DATASTRUCT_H

#include <petsc.h>

extern PetscMPIInt  rank,size;
extern PetscInt	    n;
extern PetscInt     nstart;
extern PetscInt     nstop;
extern PetscInt     save_interval;
extern PetscInt     num_save;
extern PetscInt	    yes_tecplot;
extern PetscInt     nx,ny;
extern PetscReal    dt;

extern PetscInt       xs,xw,ys,yw;
extern DM             da;
extern DMBoundaryType bx,by;
extern Mat            A;
extern Vec            bv,solv;
extern KSP            ksp;
extern PC             pcksp;
extern PetscInt       firstx,firsty,lastx,lasty;
extern PetscInt       global_firstx,global_firsty,global_lastx,global_lasty;
extern PetscReal      lx,ly,dx,dy;
extern PetscReal      sim_time;
extern PetscInt       t;
extern MPI_Datatype   xface3D,yface3D,xface2D,yface2D;
extern PetscMPIInt    nbrleft,nbrright,nbrbottom,nbrtop; 
extern PetscReal      max_drho_global,max_dgx_global,max_dgy_global,max_dg_global;
extern PetscInt       not_converged;
extern PetscReal      ksp_tol,tolerance_drho,tolerance_dmom;

extern PetscInt     BCxL,BCyL,BCxR,BCyR;

extern double       *xMem,*yMem,*rhoMem,*gxMem,*gyMem,*uMem,*vMem,*uxNMem,*vyNMem;
extern double       *rhosMem,*gxsMem,*gysMem;
extern double       **x,**y,***rho,***gx,***gy,***u,***v,***uxN,***vyN;
extern double       **rhos,**gxs,**gys;
extern double       *rho_in,*rho_out,*rho_bot,*rho_top;
extern double       *gx_in,*gx_out,*gx_bot,*gx_top;
extern double       *gy_in,*gy_out,*gy_bot,*gy_top;
extern double       **b,**sol;

#endif