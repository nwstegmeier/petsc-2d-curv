#ifndef _DRIVER_H
#define _DRIVER_H

#include "datastruct.h"

// solves NS eqns
extern void solve_NSCH();

// initalizes PETSc objects
extern void set_up_Petsc();

// allocates memory for 2D and 3D arrays
extern void allocate_array_memory();

// sets the initial conditions
extern void init_cond();

// solves for density
extern void advance_continuity();

// solves for momentum-y
extern void advance_gx();

// solves for momentum-x
extern void advance_gy();

// sets outermost boundary conditions 
extern void enforce_ghost_by_bc();

// f(t-2)=f(t-1); f(t-1)=f(t);
extern void time_forward();

// computes the i+1/2 face velocity from average of nodes
extern void compute_face_vel();

// updates ghost points for 3D vector using MPI
extern void update_ghost_3D(double ***a,int t_idx);

// updates ghost points for 2D vector using MPI
extern void update_ghost_2D(double **a);

// copies t timestep into t-1 timestep 
extern void store_current_ts(double **as, double ***a);

// monitors convergence of coupled NS eqns
extern void check_convergence();

// sets boundary values (e.g., rho_in/rho_out) for b.c.
extern void set_boundary_values();

// updates boundary conditions ( 1=periodic, 2=dirichlet, 3=neumann)
extern void update_bc(int i, int j, double ***ff, double *ff_in, double *ff_out, double *ff_bot, double *ff_top,
                      double   apg,  double &rhsg, double   bpg, double &apbg, double brsg,
                      double &aexg,  double &aeyg, double &awxg, double &awyg,
                      double  bexg, double   beyg, double  bwxg,  double bwyg,
                      int BCxL, int BCyL, int BCxR, int BCyR);

// updates outer boundary conditions
extern void update_outer_bc(double ***ff, double *ff_in, double *ff_out, double *ff_bot,double *ff_top,
                              int BCxL, int BCyL, int BCxR, int BCyR);
                              
#endif