#include "driver.h"
#include "xml.h"
#include <petsc.h>

void solve_NSCH(){
  PetscInt iter_main_loop;
  
	init_cond();
  set_boundary_values();
  enforce_ghost_by_bc();
  save_VTK_ASCII();
  
  PetscPrintf(PETSC_COMM_WORLD,"\n Beginning main loop... \n");

	for (n=nstart; n<nstop+1; n++){		
    iter_main_loop = 0;
    
    while (not_converged && iter_main_loop<200){
      iter_main_loop += 1;
      
      advance_continuity();   
      advance_gx();
      advance_gy();
      check_convergence();
      
      PetscPrintf(PETSC_COMM_WORLD,"%5i %5i %12.4e %12.4e %12.4e\n",n,iter_main_loop,
                         max_drho_global,max_dgx_global,max_dgy_global);
                              
    }
    
		if(n%save_interval==0){
      save_VTK_ASCII();
      num_save=num_save+1;
    }	
    
    time_forward();
    not_converged=1;
    
	}
  
};

void set_up_Petsc(){
  const PetscMPIInt   *neighbors;
	DMDAStencilType 	  stype=DMDA_STENCIL_BOX;
	PetscInt 			      dof=1,sw=1;
			
  bx=DM_BOUNDARY_PERIODIC;
  by=DM_BOUNDARY_GHOSTED;
  BCxL=0;BCyL=0;BCxR=1;BCyR=1;
      
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,dof,sw,NULL,NULL,&da);
  DMSetFromOptions(da);
  DMSetUp(da);
	DMDAGetCorners(da,&xs,&ys,0,&xw,&yw,0);
	DMView(da,PETSC_VIEWER_STDOUT_WORLD);
  
  DMCreateGlobalVector(da,&bv);
  DMCreateGlobalVector(da,&solv);	
  VecSetUp(bv); 
  VecSetUp(solv);
  VecSetFromOptions(bv); 
  VecSetFromOptions(solv); 
  
  DMCreateMatrix(da,&A);
	MatSetFromOptions(A);
	MatSetUp(A);

  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetType(ksp,KSPGMRES);
  KSPGetPC(ksp,&pcksp);
  PCSetType(pcksp,PCJACOBI);
  KSPSetPC(ksp,pcksp);
  KSPSetFromOptions(ksp);
  KSPSetOperators(ksp,A,A);
  KSPSetUp(ksp);
   
  KSPSetTolerances(ksp,ksp_tol,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);
  
  // determine neighboring processors
  DMDAGetNeighbors(da,&neighbors);
  nbrleft		= neighbors[3];
  nbrright	= neighbors[5];
  nbrbottom	= neighbors[1];
  nbrtop		= neighbors[7];
  if(nbrleft<0){nbrleft=MPI_PROC_NULL;}
  if(nbrright<0){nbrright=MPI_PROC_NULL;}
  if(nbrbottom<0){nbrbottom=MPI_PROC_NULL;}
  if(nbrtop<0){nbrtop=MPI_PROC_NULL;}
	
  // define the "interior" domain
  firstx=1; 
  firsty=1;
  lastx=xw;
  lasty=yw;
  
  // define location in global domain
  global_firstx = xs+1;
  global_lastx  = xs+xw;
  global_firsty = ys+1;
  global_lasty  = ys+yw;
    
  // create custom MPI data-type for exchanging xface boundaries
  MPI_Type_vector(lasty-firsty+3,1,(lastx-firstx+3),MPI_DOUBLE_PRECISION,&xface2D);
  MPI_Type_commit(&xface2D);
  MPI_Type_vector(lasty-firsty+3,1,(lastx-firstx+3)*3,MPI_DOUBLE_PRECISION,&xface3D);
  MPI_Type_commit(&xface3D);
  MPI_Type_vector((lastx-firstx+3),1,3,MPI_DOUBLE_PRECISION,&yface3D);
  MPI_Type_commit(&yface3D);
  
};

void allocate_array_memory(){
  PetscInt i,j;
  
  // allocate 1D arrays
  rho_in   = new double[lasty-firsty+3];
  rho_out  = new double[lasty-firsty+3];
  rho_bot = new double[lastx-firstx+3];
  rho_top = new double[lastx-firstx+3];  
  gx_in   = new double[lasty-firsty+3];
  gx_out  = new double[lasty-firsty+3];
  gx_bot = new double[lastx-firstx+3];
  gx_top = new double[lastx-firstx+3];  
  gy_in   = new double[lasty-firsty+3];
  gy_out  = new double[lasty-firsty+3];
  gy_bot = new double[lastx-firstx+3];
  gy_top = new double[lastx-firstx+3];  
  
  // allocate 2D arrays
  xMem    = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  yMem    = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  rhosMem = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  gxsMem = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  gysMem = (double *) malloc(sizeof(double)*(lasty-firsty+3)*(lastx-firstx+3));
  
  x       = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  y       = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  rhos    = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  gxs    = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  gys    = (double **) malloc(sizeof(double *)*(lasty-firsty+3));
  
  for(j=firsty-1; j<=lasty+1; j++){
    x[j]    = xMem + (lastx-firstx+3)*j;
    y[j]    = yMem + (lastx-firstx+3)*j;
    rhos[j] = rhosMem + (lastx-firstx+3)*j;
    gxs[j]  = gxsMem + (lastx-firstx+3)*j;
    gys[j]  = gysMem + (lastx-firstx+3)*j;
  }
  
  // allocate 3D arrays
  rhoMem  = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  gxMem   = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  gyMem   = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  uMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  vMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  uxNMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  vyNMem    = new double[(lasty-firsty+3)*(lastx-firstx+3)*3];
  
  rho = new double**[lasty-firsty+3];
  gx  = new double**[lasty-firsty+3];
  gy  = new double**[lasty-firsty+3];
  u   = new double**[lasty-firsty+3];
  v   = new double**[lasty-firsty+3];
  uxN = new double**[lasty-firsty+3];
  vyN = new double**[lasty-firsty+3];
  
  for(j=firsty-1; j<=lasty+1; j++){
    rho[j]  = new double*[lastx-firstx+3];
    gx[j]   = new double*[lastx-firstx+3];
    gy[j]   = new double*[lastx-firstx+3];
    u[j]    = new double*[lastx-firstx+3];
    v[j]    = new double*[lastx-firstx+3];
    uxN[j]  = new double*[lastx-firstx+3];
    vyN[j]  = new double*[lastx-firstx+3];
    for (i=firstx-1; i<=lastx+1; i++){
      rho[j][i] = rhoMem + j*(lastx-firstx+3)*3 + i*3;
      gx[j][i]  = gxMem  + j*(lastx-firstx+3)*3 + i*3;
      gy[j][i]  = gyMem  + j*(lastx-firstx+3)*3 + i*3;
      u[j][i]   = uMem   + j*(lastx-firstx+3)*3 + i*3;
      v[j][i]   = vMem   + j*(lastx-firstx+3)*3 + i*3;
      uxN[j][i] = uxNMem + j*(lastx-firstx+3)*3 + i*3;
      vyN[j][i] = vyNMem + j*(lastx-firstx+3)*3 + i*3;
    }
  }
  
}

void init_cond(){
  PetscInt i,j;
  
  lx = 2.0*PETSC_PI;
  ly = 2.0*PETSC_PI;
  dx = lx/nx;
  dy = ly/ny;
  n=1;
  sim_time = 0.0;
  
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    x[j][i]= -dx/2.0 + (float)(i+xs)*dx;
    y[j][i]= -dy/2.0 + (float)(j+ys)*dy;
    rho[j][i][t] = 1.0;
    u[j][i][t] = 0.0;
    v[j][i][t] = PetscSinReal(y[j][i]);
    gx[j][i][t] = u[j][i][t]*rho[j][i][t];
    gy[j][i][t] = v[j][i][t]*rho[j][i][t];
  }
  }
      
  PetscPrintf(PETSC_COMM_WORLD,"\n---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"-------------- Sim Params -------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"rank=%d, size=%d\n",rank,size);
  PetscPrintf(PETSC_COMM_WORLD,"nstart=%i,nstop=%i,save_interval=%i\n",nstart,nstop,save_interval);
  PetscPrintf(PETSC_COMM_WORLD,"num_save=%i,yes_tecplot=%i\n",num_save,yes_tecplot);
  PetscPrintf(PETSC_COMM_WORLD,"nx=%d,ny=%d\n",nx,ny);
  PetscPrintf(PETSC_COMM_WORLD,"lx=%f,ly=%f\n",lx,ly);
  PetscPrintf(PETSC_COMM_WORLD,"dx=%f,dy=%f\n",dx,dy);
  PetscPrintf(PETSC_COMM_WORLD,"dt=%f\n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"firstx=%d,lastx=%d\n",firstx,lastx);
  PetscPrintf(PETSC_COMM_WORLD,"global_firstx=%d,global_lastx=%d\n",global_firstx,global_lastx);
  PetscPrintf(PETSC_COMM_WORLD,"continuity tolerance=%f\n",tolerance_drho);
  PetscPrintf(PETSC_COMM_WORLD,"momentum tolerance=%f\n",tolerance_dmom);
  PetscPrintf(PETSC_COMM_WORLD,"Initial conditions saved... \n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n\n");
  
  for(i=0; i<=size; i++){
    if(i==rank){
      PetscPrintf(PETSC_COMM_SELF,"rank=%d, size=%d\n",rank,size);
      PetscPrintf(PETSC_COMM_SELF,"nbrleft=%d, nbrright=%d\n",nbrleft,nbrright);
      PetscPrintf(PETSC_COMM_SELF,"nbrtop=%d, nbrbottom=%d\n",nbrtop,nbrbottom);
    }
    MPI_Barrier(PETSC_COMM_WORLD);
  }
  
};

void update_ghost_3D(double ***a, int t_idx = 2){ 
  MPI_Status status; 

  //--------------------- 3D Array Case --------------------------------
  // exchange ghost points on y faces
  MPI_Sendrecv(&a[lasty   ][firstx-1][t_idx],1,yface3D,nbrtop   ,0, 
               &a[firsty-1][firstx-1][t_idx],1,yface3D,nbrbottom,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty  ][firstx-1][t_idx],1,yface3D,nbrbottom,1, 
               &a[lasty+1 ][firstx-1][t_idx],1,yface3D,nbrtop   ,1, 
               PETSC_COMM_WORLD,&status);

  // exchange ghost points on x faces
  MPI_Sendrecv(&a[firsty-1][lastx   ][t_idx],1,xface3D,nbrright,0, 
               &a[firsty-1][firstx-1][t_idx],1,xface3D,nbrleft ,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty-1][firstx  ][t_idx],1,xface3D,nbrleft ,1, 
               &a[firsty-1][lastx+1 ][t_idx],1,xface3D,nbrright,1, 
               PETSC_COMM_WORLD,&status);

  MPI_Barrier(PETSC_COMM_WORLD);
               
}

void update_ghost_2D(double **a){ 
  MPI_Status status; 

  //--------------------- 2D Array Case --------------------------------  
  // exchange ghost points on y faces
  MPI_Sendrecv(&a[lasty   ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrtop   ,0,  \
               &a[firsty-1][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrbottom,0,  \
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty  ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrbottom,1,  \
               &a[lasty+1 ][firstx-1],(lastx-firstx+3),MPI_DOUBLE_PRECISION,nbrtop   ,1,  \
               PETSC_COMM_WORLD,&status);


  // exchange ghost points on x faces
  MPI_Sendrecv(&a[firsty-1][lastx   ],1,xface2D,nbrright,0, 
               &a[firsty-1][firstx-1],1,xface2D,nbrleft ,0, 
               PETSC_COMM_WORLD,&status);

  MPI_Sendrecv(&a[firsty-1][firstx  ],1,xface2D,nbrleft ,1, 
               &a[firsty-1][lastx+1 ],1,xface2D,nbrright,1, 
               PETSC_COMM_WORLD,&status);  

  MPI_Barrier(PETSC_COMM_WORLD);
               
}             

void enforce_ghost_by_bc(){
  PetscInt i,j,k;
  
  update_outer_bc(rho,rho_in,rho_out,rho_bot,rho_top,BCxL,BCyL,BCxR,BCyR);
  update_outer_bc( gx, gx_in, gx_out, gx_bot, gx_top,BCxL,BCyL,BCxR,BCyR);
  update_outer_bc( gy, gy_in, gy_out, gy_bot, gy_top,BCxL,BCyL,BCxR,BCyR);
  
  update_ghost_3D(rho,t);
  update_ghost_3D(gx,t);
  update_ghost_3D(gy,t);
  update_ghost_3D(u,t);
  update_ghost_3D(v,t);
  
  compute_face_vel();
  
  update_ghost_3D(uxN,t);
  update_ghost_3D(vyN,t);
  
  // initial time_forward
  for(k=2; k>=1; k--){
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    rho[j][i][k-1] = rho[j][i][k];
    gx[j][i][k-1] = gx[j][i][k];
    gy[j][i][k-1] = gy[j][i][k];
    u[j][i][k-1] = u[j][i][k];
    v[j][i][k-1] = v[j][i][k];
    uxN[j][i][k-1] = uxN[j][i][k];
    vyN[j][i][k-1] = vyN[j][i][k];
  }
  }
  }
    
}

void time_forward(){
  PetscInt i,j,k;
  for(k=2; k>=1; k--){
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    rho[j][i][k-1] = rho[j][i][k];
    gx[j][i][k-1] = gx[j][i][k];
    gy[j][i][k-1] = gy[j][i][k];
    u[j][i][k-1] = u[j][i][k];
    v[j][i][k-1] = v[j][i][k];
    uxN[j][i][k-1] = uxN[j][i][k];
    vyN[j][i][k-1] = vyN[j][i][k];
  }
  }
  }
  
  sim_time+=dt;
  
}

void compute_face_vel(){
  PetscInt i,j;
  for(j=firsty; j<=lasty+1; j++){
  for(i=firstx; i<=lastx+1; i++){
    //uxN[j][i][t] = 0.5*(u[j  ][i-1][t]+u[j][i][t]);
    //vyN[j][i][t] = 0.5*(v[j-1][i  ][t]+v[j][i][t]);
    uxN[j][i][t]=0.0;
    vyN[j][i][t]=1.0;
  }
  }
}  

void store_current_ts(double **as, double ***a){
  PetscInt i,j;
  for(j=firsty-1; j<=lasty+1; j++){
  for(i=firstx-1; i<=lastx+1; i++){
    as[j][i]=a[j][i][t];
  }
  }
}

void check_convergence(){
  PetscInt  i,j;
  PetscReal max_drho_local,max_dgx_local,max_dgy_local;
  
  max_drho_local = 0.0;
  max_dgx_local = 0.0;
  max_dgy_local = 0.0;
  
  for(j=firsty; j<=lasty; j++){
  for(i=firstx; i<=lastx; i++){
    max_drho_local = PetscMax(max_drho_local,PetscAbsReal(rho[j][i][t]-rhos[j][i]));
    max_dgx_local = PetscMax(max_dgx_local,PetscAbsReal(gx[j][i][t]-gxs[j][i]));
    max_dgy_local = PetscMax(max_dgy_local,PetscAbsReal(gy[j][i][t]-gys[j][i]));
  }
  }
  
  MPI_Allreduce(&max_drho_local,&max_drho_global,1,MPI_DOUBLE_PRECISION,MPI_MAX,PETSC_COMM_WORLD);
  MPI_Allreduce(&max_dgx_local,&max_dgx_global,1,MPI_DOUBLE_PRECISION,MPI_MAX,PETSC_COMM_WORLD);
  MPI_Allreduce(&max_dgy_local,&max_dgy_global,1,MPI_DOUBLE_PRECISION,MPI_MAX,PETSC_COMM_WORLD);
  
  max_dg_global=PetscMax(max_dgx_global,max_dgy_global);
  
  if(max_drho_global < tolerance_drho && 
     max_dg_global   < tolerance_dmom){  //&& 
        not_converged=0; // not not converged -> converged
  }
    
}

// function to build rho_in, rho_bot...
void set_boundary_values(){
  PetscInt i,j;
  for(j=firsty-1; j<=lasty+1; j++){
    rho_in[j]   = 0.5*(rho[j][firstx-1][t] + rho[j][firstx][t]);
    gx_in[j]    = 0.5*( gx[j][firstx-1][t] +  gx[j][firstx][t]);
    gy_in[j]    = 0.5*( gy[j][firstx-1][t] +  gy[j][firstx][t]);
    rho_out[j]  = 0.5*(rho[j][lastx][t] + rho[j][lastx+1][t]);
    gx_out[j]   = 0.5*( gx[j][lastx][t] +  gx[j][lastx+1][t]);
    gy_out[j]   = 0.5*( gy[j][lastx][t] +  gy[j][lastx+1][t]);
    
  }
  for(i=firstx-1; i<=lastx+1; i++){
    rho_bot[i]  = 0.5*(rho[firsty-1][i][t] + rho[firsty][i][t]);
    gx_bot[i]   = 0.5*( gx[firsty-1][i][t] +  gx[firsty][i][t]);
    gy_bot[i]   = 0.5*( gy[firsty-1][i][t] +  gy[firsty][i][t]);
    rho_top[i]  = 0.5*(rho[lasty][i][t] + rho[lasty+1][i][t]);
    gx_top[i]   = 0.5*( gx[lasty][i][t] +  gx[lasty+1][i][t]);
    gy_top[i]   = 0.5*( gy[lasty][i][t] +  gy[lasty+1][i][t]);
  }
}

void update_outer_bc(double ***ff, double *ff_in, double *ff_out, double *ff_bot,double *ff_top, 
                     int BCxL, int BCyL, int BCxR, int BCyR)
{
  // 0 is const. value
  // 1 is d/dx = 0
  PetscInt i,j;
  
  if(bx != DM_BOUNDARY_PERIODIC){
    if(BCxL==0){ if(global_firstx==1){ for(j=firsty-1; j<=lasty+1; j++){
        ff[j][firstx-1][t]=2.0*ff_in[j]-ff[j][firstx][t];
    }}}
    
    if(BCxL==1){ if(global_firstx==1){ for(j=firsty-1; j<=lasty+1; j++){
        ff[j][firstx-1][t]=ff[j][firstx][t];
    }}}
    
    if(BCxR==0){ if(global_lastx==nx){ for(j=firsty-1; j<=lasty+1; j++){
        ff[j][lastx+1][t]=2.0*ff_out[j]-ff[j][lastx][t];
    }}}
    
    if(BCxR==1){ if(global_lastx==nx){ for(j=firsty-1; j<=lasty+1; j++){
        ff[j][lastx+1][t]=ff[j][lastx][t];
    }}} 
  }
  
  if(by != DM_BOUNDARY_PERIODIC){
    if(BCyL==0){ if(global_firsty==1){ for(i=firstx-1; i<=lastx+1; i++){
        ff[firsty-1][i][t]=2.0*ff_bot[i]-ff[firsty][i][t];
    }}}
    
    if(BCyL==1){ if(global_firsty==1){ for(i=firstx-1; i<=lastx+1; i++){
        ff[firsty-1][i][t]=ff[firsty][i][t];
    }}}
    
    if(BCyR==0){ if(global_lasty==ny){ for(i=firstx-1; i<=lastx+1; i++){
        ff[lasty+1][i][t]=2.0*ff_top[i]-ff[lasty][i][t];
    }}}
    
    if(BCyR==1){ if(global_lasty==ny){ for(i=firstx-1; i<=lastx+1; i++){
        ff[lasty+1][i][t]=ff[lasty][i][t];
    }}}
  }
}

void update_bc(int i, int j, double ***ff, double *ff_in, double *ff_out, double *ff_bot, double *ff_top,
                double   apg, double &rhsg, double   bpg, double &apbg, double brsg,
                double &aexg, double &aeyg, double &awxg, double &awyg,
                double  bexg, double  beyg, double  bwxg, double  bwyg,
                int BCxL, int BCyL, int BCxR, int BCyR)
{
  if(bx != DM_BOUNDARY_PERIODIC){
    if(BCxL==0){if(i+xs==1){
      apbg=apbg-awxg;
      rhsg=rhsg-bwxg*ff[j][i-1][t-1]-bwxg*ff[j][i][t-1]+(2.0*bwxg-2.0*awxg)*ff_in[j];
    }}
    if(BCxL==1){if(i+xs==1){
      apbg=apbg+awxg;
      rhsg=rhsg-bwxg*ff[j][i-1][t-1]+bwxg*ff[j][i][t-1];
    }}
    if(BCxR==0){if(i+xs==nx){
      apbg=apbg-aexg;
      rhsg=rhsg-bexg*ff[j][i+1][t-1]-bexg*ff[j][i][t-1]+(2.0*bexg-2.0*aexg)*ff_out[j];   
    }}
    if(BCxR==1){if(i+xs==nx){
      apbg=apbg+aexg;
      rhsg=rhsg-bexg*ff[j][i+1][t-1]+bexg*ff[j][i][t-1];   
    }}
  }
  if(by != DM_BOUNDARY_PERIODIC){
    if(BCyL==0){if(j+ys==1){
      apbg=apbg-awyg;
      rhsg=rhsg-bwyg*ff[j-1][i][t-1]-bwyg*ff[j][i][t-1]+(2.0*bwyg-2.0*awyg)*ff_bot[i];
    }}
    if(BCyL==1){if(j+ys==1){
      apbg=apbg+awyg;
      rhsg=rhsg-bwyg*ff[j-1][i][t-1]+bwyg*ff[j][i][t-1];
    }}
    if(BCyR==0){if(j+ys==ny){
      apbg=apbg-aeyg;
      rhsg=rhsg-beyg*ff[j+1][i][t-1]-beyg*ff[j][i][t-1]+(2.0*beyg-2.0*aeyg)*ff_top[i];
    }}
    if(BCyR==1){if(j+ys==ny){
      apbg=apbg+aeyg;
      rhsg=rhsg-beyg*ff[j+1][i][t-1]+beyg*ff[j][i][t-1];
    }}
  }
}

