#ifndef _DATASTRUCT_H
#define _DATASTRUCT_H

#include <petsc.h>

extern PetscMPIInt rank,size;
extern PetscInt	n;
extern PetscInt nstart;
extern PetscInt nstop;
extern PetscInt save_interval;
extern PetscInt num_save;
extern PetscInt	yes_tecplot;

extern PetscInt nx,ny;
extern PetscInt xs,xw,ys,yw;
extern PetscReal lx,ly,dx,dy,dt,xp,yp;
extern PetscReal sim_time;
extern DM da,daKSP;
extern Vec gv,lv,bv,X,lX;
extern Vec xv,yv,uxNv,vyNv;
extern PetscScalar **b, **x, **y, ***uxN, ***vyN;
extern Mat A;
extern KSP ksp;
extern PC pcksp;
typedef struct {PetscScalar d,u,v,gx,gy;} Field;
extern Field **G,**L;

#endif