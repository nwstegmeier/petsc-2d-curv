#include <petsc.h>
#include "datastruct.h"
#include "driver.h"

PetscMPIInt     rank,size,ierr;
PetscInt		    n;
PetscInt		    nstart;
PetscInt 		    nstop;
PetscInt 		    save_interval;
PetscInt 		    num_save;
PetscInt		    yes_tecplot;

PetscInt 		    nx,ny;
PetscInt 		    xs,xw,ys,yw;
PetscReal 	        lx,ly,dx,dy,dt,xp,yp;
PetscReal		    sim_time;
PetscReal           Re = 1000;
DM 				    da,daKSP;
Vec 			    gv,lv,bv,X,lX,Dv,lDv;
Vec 			    xv,yv,uxNv,vyNv;
Mat 			    A;
KSP 			    ksp;
PC 				    pcksp;
PetscScalar         **b,**x,**y,***uxN,***vyN,**D;
Field 			    **G,**L;

int main(int argc,char **argv){
	
  PetscInitialize(&argc,&argv,(char*)0,NULL);
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  
  PetscOptionsGetInt(NULL,NULL,"-nstart",&nstart,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nstop",&nstop,NULL);
  PetscOptionsGetInt(NULL,NULL,"-saveint",&save_interval,NULL);
  PetscOptionsGetInt(NULL,NULL,"-num_save",&num_save,NULL);
  PetscOptionsGetInt(NULL,NULL,"-yes_tecplot",&yes_tecplot,NULL);
  PetscOptionsGetReal(NULL,NULL,"-dt",&dt,NULL);
  PetscOptionsGetInt(NULL,NULL,"-nx",&nx,NULL);
  PetscOptionsGetInt(NULL,NULL,"-ny",&ny,NULL);
  
  set_up_Petsc();
  solve_NSKH();

  PetscPrintf(PETSC_COMM_WORLD,"Simulation complete.\n\n"); 
  
  VecDestroy(&gv);
  VecDestroy(&lv);
  VecDestroy(&bv);
  VecDestroy(&Dv);
  VecDestroy(&uxNv);
  VecDestroy(&vyNv);
  VecDestroy(&xv);
  VecDestroy(&yv);
  VecDestroy(&X);
  VecDestroy(&lX);
  MatDestroy(&A);
  DMDestroy(&da);
  DMDestroy(&daKSP);
  KSPDestroy(&ksp);
  PetscFinalize();
  
  return(ierr);
		
}


