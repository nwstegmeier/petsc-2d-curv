#!/bin/bash

nstart=0
nstop=10000
saveint=100
num_save=0
yes_tecplot=0
dt=0.001
nx=30
ny=30

mpiexec -n 1 ./run -nx $nx -ny $ny -dt $dt -nstart $nstart -nstop $nstop -saveint $saveint -num_save $num_save -yes_tecplot $yes_tecplot

exit
