#include "driver.h"
#include <petsc.h>


// ---------------------------------------------------------------------
// main solver function
// ---------------------------------------------------------------------
void solve_NSKH(){
	
  // set the initial conditions
	init_cond();
	
  // time evolution
	for (n=nstart; n<=nstop; n++){
		
	//advance_continuity();
    advance_gx();
    advance_gy();
    update_cell_vel();
    //advance_p();
    taylor_pressure_exact();
    
    store_face_vel();
    compute_face_vel();
    
    //update_cv_mom_and_vel();

    time_forward();
    
    // STDOUT and saving data
		if(n%save_interval==0 && n>0){
      PetscPrintf(PETSC_COMM_WORLD,"timestep %5i, save %5i \n",n,num_save);
			save_data();
		} else {
      PetscPrintf(PETSC_COMM_WORLD,"timestep %5i \n",n);
    }
				
	}
	
};
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

void set_up_Petsc(){
	DMBoundaryType 		bx=DM_BOUNDARY_PERIODIC;
	DMBoundaryType 		by=DM_BOUNDARY_PERIODIC;
	DMDAStencilType 	stype=DMDA_STENCIL_BOX;
	PetscInt 			dof = 6,sw = 1, ione=1;
			
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,dof,sw,NULL,NULL,&da);
    DMSetFromOptions(da);
    DMSetUp(da);
	DMCreateGlobalVector(da,&gv);
	DMCreateLocalVector(da,&lv);
	DMDAGetCorners(da,&xs,&ys,0,&xw,&yw,0);
	DMView(da,PETSC_VIEWER_STDOUT_WORLD);
	
	DMDACreate2d(PETSC_COMM_WORLD,bx,by,stype,nx,ny,PETSC_DECIDE,PETSC_DECIDE,ione,sw,NULL,NULL,&daKSP);
    DMSetFromOptions(daKSP);
    DMSetUp(daKSP);
	DMCreateGlobalVector(daKSP,&bv);
	DMCreateGlobalVector(daKSP,&X);
    DMCreateGlobalVector(daKSP,&Dv);
	DMCreateLocalVector(daKSP,&lX);
    DMCreateLocalVector(daKSP,&lDv);
    VecSetUp(bv); VecSetUp(X); VecSetUp(lX); VecSetUp(Dv); VecSetUp(lDv);
    VecSetFromOptions(bv); 
    VecSetFromOptions(X); 
    VecSetFromOptions(lX);
    VecSetFromOptions(Dv); 
    VecSetFromOptions(lDv);
	DMCreateMatrix(daKSP,&A);
	MatSetFromOptions(A);
	MatSetUp(A);
	
	VecCreateMPI(PETSC_COMM_SELF,(yw+2)*(xw+2),(ny+2)*(nx+2),&xv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+2)*(xw+2),(ny+2)*(nx+2),&yv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+1)*(xw+1)*2,(ny+1)*(nx+1)*2,&uxNv);
	VecCreateMPI(PETSC_COMM_SELF,(yw+1)*(xw+1)*2,(ny+1)*(nx+1)*2,&vyNv);
  
    VecSetUp(xv);
    VecSetUp(yv);
    VecSetUp(uxNv);
    VecSetUp(vyNv);
    VecSetFromOptions(xv);
    VecSetFromOptions(yv);
    VecSetFromOptions(uxNv);
    VecSetFromOptions(vyNv);
	
	KSPCreate(PETSC_COMM_WORLD,&ksp);
	KSPSetType(ksp,KSPGMRES);
    KSPGetPC(ksp,&pcksp);
	PCSetType(pcksp,PCJACOBI);
	KSPSetPC(ksp,pcksp);
	KSPSetFromOptions(ksp);
	KSPSetOperators(ksp,A,A);
	KSPSetUp(ksp);
		
};

void init_cond(){ // currently set to advect sine wave in momentum
  PetscInt i,j;
  
  lx = 2.0*PETSC_PI;
  ly = 2.0*PETSC_PI;
  dx = lx/float(nx);
  dy = ly/float(ny);
  n=nstart;
  sim_time = 0.0;
  
  // get array for x,y
  VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
  for (j = ys-1; j < ys+yw+1; j++){ 
  for (i = xs-1; i < xs+xw+1; i++){ 
  	x[j][i]= -dx/2.0 + float(i+1)*dx;
  	y[j][i]= -dy/2.0 + float(j+1)*dy;
  }
  }
  VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
  
  VecAssemblyBegin(xv);
  VecAssemblyEnd(xv);
  VecAssemblyBegin(yv);
  VecAssemblyEnd(yv);

  // set initial primitive variables 
  VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
  DMDAVecGetArray(da,gv,&G);
  for (j = ys; j < ys+yw; j++){ 
  for (i = xs; i < xs+xw; i++){ 
  
    // density sine wave
    /*G[j][i].d = PetscSinReal(x[j][i]);		
    G[j][i].u = 1.0;
    G[j][i].v = 0.0;
    G[j][i].gx = G[j][i].d*G[j][i].u;
    G[j][i].gy = G[j][i].d*G[j][i].v;
    G[j][i].p = 0.0;*/
    
    // momentum sine wave
    /* G[j][i].d = 1.0; //PetscSinReal(x[j][i]);		
    G[j][i].u = PetscSinReal(x[j][i])*PetscSinReal(x[j][i]);
    G[j][i].v = 0.0;
    G[j][i].gx = G[j][i].d*G[j][i].u;
    G[j][i].gy = G[j][i].d*G[j][i].v;
    G[j][i].p = 0.0; */
    
    // taylor problem
    G[j][i].d = 1.0;	
    G[j][i].u = -PetscCosReal(x[j][i])*PetscSinReal(y[j][i]);
    G[j][i].v = PetscSinReal(x[j][i])*PetscCosReal(y[j][i]);
    G[j][i].gx = G[j][i].d*G[j][i].u;
    G[j][i].gy = G[j][i].d*G[j][i].v;
    G[j][i].p = -0.25*(PetscCosReal(2.0*x[j][i])+PetscCosReal(2.0*y[j][i]))*PetscExpReal(-4.0*(1.0/Re)*(dt+3.0/2.0));
    
  }
  }
  DMDAVecRestoreArray(da,gv,&G);
  VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);

  VecAssemblyBegin(gv);
  VecAssemblyEnd(gv);
  
  // update ghost points
  DMGlobalToLocalBegin(da,gv,INSERT_VALUES,lv);
  DMGlobalToLocalEnd(da,gv,INSERT_VALUES,lv);
    
  compute_duxduy();
  compute_face_vel();
  save_data();
  
  VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  for (j = ys; j < ys+yw+1; j++){ 
  for (i = xs; i < xs+xw+1; i++){
  	uxN[j][i][0]=uxN[j][i][1];
  	vyN[j][i][0]=vyN[j][i][1];
  }
  }
  VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN); 
  
  VecAssemblyBegin(uxNv);
  VecAssemblyEnd(uxNv);
  VecAssemblyBegin(vyNv);
  VecAssemblyEnd(vyNv);
  
  // print out simulation data
  PetscPrintf(PETSC_COMM_WORLD,"\n---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"-------------- Sim Params -------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"rank=%d, size=%d\n",rank,size);
  PetscPrintf(PETSC_COMM_WORLD,"nstart=%i,nstop=%i,save_interval=%i\n",nstart,nstop,save_interval);
  PetscPrintf(PETSC_COMM_WORLD,"num_save=%i,yes_tecplot=%i\n",num_save,yes_tecplot);
  PetscPrintf(PETSC_COMM_WORLD,"xs=%d,ys=%d\n",xs,ys);
  PetscPrintf(PETSC_COMM_WORLD,"nx=%d,ny=%d\n",nx,ny);
  PetscPrintf(PETSC_COMM_WORLD,"lx=%f,ly=%f\n",lx,ly);
  PetscPrintf(PETSC_COMM_WORLD,"dx=%f,dy=%f\n",dx,dy);
  PetscPrintf(PETSC_COMM_WORLD,"dt=%f\n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"Initial conditions saved... \n",dt);
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n");
  PetscPrintf(PETSC_COMM_WORLD,"---------------------------------------\n\n");
	
};

void advance_continuity(){
	PetscInt	i,j;
	PetscReal	ap,aex,awx,aey,awy,rhs,vals[5];
    PetscReal bpf,bexf,bwxf,beyf,bwyf;
	MatStencil 	row,col[5];

	DMDAVecGetArray(da,lv,&L);
	DMDAVecGetArray(daKSP,bv,&b);
	VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
	PetscMemzero(col,5*sizeof(MatStencil));
	
	for (j = ys; j < ys+yw; j++){
	for (i = xs; i < xs+xw; i++){
		
	  ap  = 1 + 0.25*( (dt/dx)*(uxN[j][i+1][1]-uxN[j][i][1]) + (dt/dy)*(vyN[j+1][i][1]-vyN[j][i][1]) );
	  aex =     0.25*(dt/dx)*uxN[j  ][i+1][1];      
	  awx =    -0.25*(dt/dx)*uxN[j  ][i  ][1];
	  aey =     0.25*(dt/dy)*vyN[j+1][i  ][1];	
	  awy =    -0.25*(dt/dy)*vyN[j  ][i  ][1];
            
      bpf  = 1.0 - 0.25*dt*( 0.0                                
                + (1.0/dx)*( uxN[j  ][i+1][1]-uxN[j][i][1] )    
                + (1.0/dy)*( vyN[j+1][i  ][1]-vyN[j][i][1] ) );
                
      bexf = -0.25*(dt/dx)*(uxN[j  ][i+1][1]);
      bwxf =  0.25*(dt/dx)*(uxN[j  ][i  ][1]);
      beyf = -0.25*(dt/dy)*(vyN[j+1][j  ][1]);
      bwyf =  0.25*(dt/dy)*(vyN[j  ][i  ][1]);
      
      rhs  =  bpf*L[j][i].d                           
           +  bexf*L[j  ][i+1].d + bwxf*L[j  ][i-1].d 
           +  beyf*L[j+1][i  ].d + bwyf*L[j-1][j  ].d;
						
			vals[0] = ap;
			vals[1] = awx;
			vals[2] = aex;
			vals[3] = awy;
			vals[4] = aey;
			b[j][i] = rhs;
			
			// Specify matrix location of elements using (i,j) location in grid	
			row.i    = i  ;    row.j = j  ; // matrix row
			col[0].i = i  ; col[0].j = j  ; // ap
			col[1].i = i-1; col[1].j = j  ; // awx
			col[2].i = i+1; col[2].j = j  ; // aex
			col[3].i = i  ; col[3].j = j-1; // awy 
			col[4].i = i  ; col[4].j = j+1; // aey
			MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
			
	}
	}
  		
	DMDAVecRestoreArray(da,lv,&L);
	DMDAVecRestoreArray(daKSP,bv,&b);
	VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
	
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
	KSPSolve(ksp,bv,X);

	DMGlobalToLocalBegin(daKSP,X,INSERT_VALUES,lX);
	DMGlobalToLocalEnd(daKSP,X,INSERT_VALUES,lX);
	VecStrideScatter(lX,0,lv,INSERT_VALUES);   // scatter into density slot
	MatZeroEntries(A);
	
};

void advance_gx(){
	PetscInt	  i,j;
	PetscReal	  ap,aex,awx,aey,awy,rhs,vals[5];
    PetscReal   bpf,bexf,bwxf,beyf,bwyf;
	MatStencil 	row,col[5];

	DMDAVecGetArray(da,lv,&L);
	DMDAVecGetArray(daKSP,bv,&b);
	VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = ys; j < ys+yw; j++){
	for (i = xs; i < xs+xw; i++){
    
    ap   =  1.0+ 0.25*(dt/dx)*( 0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0])-0.5*(uxN[j][i][1]+uxN[j][i][0]) ) 
              + 0.25*(dt/dy)*( 0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0])-0.5*(vyN[j][i][1]+vyN[j][i][0]) );
    aex  =  0.25*(dt/dx)*(0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]));
    awx  = -0.25*(dt/dx)*(0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]));
    aey  =  0.25*(dt/dy)*(0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]));
    awy  = -0.25*(dt/dy)*(0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]));

    bpf  = 1.0 - 0.25*dt*( 0.0                                                                                    
               + (1.0/dx)*(0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]) - 0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]) )    
               + (1.0/dy)*(0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]) - 0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]) ) );
          
    bexf = -0.25*(dt/dx)*( 0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]) );
    bwxf =  0.25*(dt/dx)*( 0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]) ); 
    beyf = -0.25*(dt/dy)*( 0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]) );
    bwyf =  0.25*(dt/dy)*( 0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]) );
    
    rhs  = bpf*L[j][i].gx                                                
           + bexf*L[j  ][i+1].gx + bwxf*L[j  ][i-1].gx                    
           + beyf*L[j+1][i  ].gx + bwyf*L[j-1][i  ].gx       
           
           - 0.5*(dt/dx)*(L[j][i+1].p-L[j][i-1].p)                          // gradient of pressure x-component
           + (dt/Re)*( (L[j  ][i+1].u-2*L[j][i].u+L[j  ][i-1].u)/(dx*dx) )  // d2u/dx2
           + (dt/Re)*( (L[j+1][i  ].u-2*L[j][i].u+L[j-1][i  ].u)/(dy*dy) ); // d2u/dy2      

    //PetscPrintf(PETSC_COMM_WORLD,"test! % 04.16e\n",(dt/Re)*( (L[j  ][i-1].u-2*L[j][i].u+L[j  ][i+1].u)/(dx*dx) ));
       
    vals[0] = ap;
    vals[1] = awx;
    vals[2] = aex;
    vals[3] = awy;
    vals[4] = aey;
    b[j][i] = rhs;
    //PetscPrintf(PETSC_COMM_WORLD,"test! % 04.16e % 04.16e % 04.16e % 04.16e\n",rhs,bwxf,beyf,bwyf);
    
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = i  ;    row.j = j  ; // matrix row
    col[0].i = i  ; col[0].j = j  ; // ap
    col[1].i = i-1; col[1].j = j  ; // awx
    col[2].i = i+1; col[2].j = j  ; // aex
    col[3].i = i  ; col[3].j = j-1; // awy 
    col[4].i = i  ; col[4].j = j+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
          
    //PetscPrintf(PETSC_COMM_WORLD,"%f,%f,%f,%f,%f,%f \n",ap,awx,aex,awy,aey,rhs); 
    //PetscPrintf(PETSC_COMM_WORLD,"%5.8e, %5.8e, %5.8e, %5.8e \n",uxN[j][i][1],vyN[j][i][1],L[j][i].u,L[j][i].v); 
          
	}
	}
  		
	DMDAVecRestoreArray(da,lv,&L);
	DMDAVecRestoreArray(daKSP,bv,&b);
	VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
   
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
	KSPSolve(ksp,bv,X);

	DMGlobalToLocalBegin(daKSP,X,INSERT_VALUES,lX);
	DMGlobalToLocalEnd(daKSP,X,INSERT_VALUES,lX);
	VecStrideScatter(lX,3,lv,INSERT_VALUES);   // scatter into gx slot
	MatZeroEntries(A);
  
  DMDAVecGetArray(da,lv,&L);
  for (j = ys-1; j < ys+yw+1; j++){ 
  for (i = xs-1; i < xs+xw+1; i++){
  	L[j][i].u=L[j][i].gx/L[j][i].d;
  }
  }
  DMDAVecRestoreArray(da,lv,&L);
   
}

void advance_gy(){
	PetscInt    i,j;
	PetscReal   ap,aex,awx,aey,awy,rhs,vals[5];
    PetscReal   bpf,bexf,bwxf,beyf,bwyf;
	MatStencil  row,col[5];

	DMDAVecGetArray(da,lv,&L);
	DMDAVecGetArray(daKSP,bv,&b);
	VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
	PetscMemzero(col,5*sizeof(MatStencil));
  
	for (j = ys; j < ys+yw; j++){
	for (i = xs; i < xs+xw; i++){
    
    ap   =  1.0 + 0.25*(dt/dx)*( 0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0])-0.5*(uxN[j][i][1]+uxN[j][i][0]) ) 
                + 0.25*(dt/dy)*( 0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0])-0.5*(vyN[j][i][1]+vyN[j][i][0]) );
    aex  =  0.25*(dt/dx)*(0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]));
    awx  = -0.25*(dt/dx)*(0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]));
    aey  =  0.25*(dt/dy)*(0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]));
    awy  = -0.25*(dt/dy)*(0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]));

    bpf  = 1.0 - 0.25*dt*( 0.0                                                                                    
               + (1.0/dx)*(0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]) - 0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]) )    
               + (1.0/dy)*(0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]) - 0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]) ) );
          
    bexf = -0.25*(dt/dx)*( 0.5*(uxN[j  ][i+1][1]+uxN[j  ][i+1][0]) );
    bwxf =  0.25*(dt/dx)*( 0.5*(uxN[j  ][i  ][1]+uxN[j  ][i  ][0]) ); 
    beyf = -0.25*(dt/dy)*( 0.5*(vyN[j+1][i  ][1]+vyN[j+1][i  ][0]) );
    bwyf =  0.25*(dt/dy)*( 0.5*(vyN[j  ][i  ][1]+vyN[j  ][i  ][0]) );
    
    rhs  = bpf*L[j][i].gy                               
           + bexf*L[j  ][i+1].gy + bwxf*L[j  ][i-1].gy  
           + beyf*L[j+1][i  ].gy + bwyf*L[j-1][i  ].gy
           
           - 0.5*(dt/dy)*(L[j+1][i].p-L[j-1][i].p)                        // gradient of pressure y-component
           + (dt/Re)*((L[j  ][i+1].v-2*L[j][i].v+L[j  ][i-1].v)/(dx*dx))  // d2v/dx2 NEEDS DENSITY SOMEHWERE!!!
           + (dt/Re)*((L[j+1][i  ].v-2*L[j][i].v+L[j-1][i  ].v)/(dy*dy)); // d2v/dy2
          
    vals[0] = ap;
    vals[1] = awx;
    vals[2] = aex;
    vals[3] = awy;
    vals[4] = aey;
    b[j][i] = rhs;
    //PetscPrintf(PETSC_COMM_WORLD,"test! % 04.16e % 04.16e % 04.16e % 04.16e\n",rhs,bwxf,beyf,bwyf);
    
    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = i  ;    row.j = j  ; // matrix row
    col[0].i = i  ; col[0].j = j  ; // ap
    col[1].i = i-1; col[1].j = j  ; // awx
    col[2].i = i+1; col[2].j = j  ; // aex
    col[3].i = i  ; col[3].j = j-1; // awy 
    col[4].i = i  ; col[4].j = j+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);
          
    //PetscPrintf(PETSC_COMM_WORLD,"%f,%f,%f,%f,%f,%f \n",ap,awx,aex,awy,aey,rhs); 
    //PetscPrintf(PETSC_COMM_WORLD,"%5.8e, %5.8e, %5.8e, %5.8e \n",uxN[j][i][1],vyN[j][i][1],L[j][i].u,L[j][i].v); 
          
	}
	}
  		
	DMDAVecRestoreArray(da,lv,&L);
	DMDAVecRestoreArray(daKSP,bv,&b);
	VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
	VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
   
	MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
	VecAssemblyBegin(bv);
	VecAssemblyEnd(bv);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
	KSPSolve(ksp,bv,X);

	DMGlobalToLocalBegin(daKSP,X,INSERT_VALUES,lX);
	DMGlobalToLocalEnd(daKSP,X,INSERT_VALUES,lX);
	VecStrideScatter(lX,4,lv,INSERT_VALUES);   // scatter into density slot
	MatZeroEntries(A);
  
  DMDAVecGetArray(da,lv,&L);
  for (j = ys-1; j < ys+yw+1; j++){ 
  for (i = xs-1; i < xs+xw+1; i++){
  	L[j][i].v=L[j][i].gy/L[j][i].d;
  }
  }
  DMDAVecRestoreArray(da,lv,&L);
  
}

void advance_p(){
  PetscInt	  i,j;
  PetscReal	  ap,aex,awx,aey,awy,rhs,vals[5];
  PetscReal   D2x, D2y;
  PetscReal   duud2x, dvvd2y, duvdxy;
  MatStencil  row,col[5];
    
  compute_duxduy();
    
  DMDAVecGetArray(da,lv,&L);
  DMDAVecGetArray(daKSP,bv,&b);
  DMDAVecGetArray(daKSP,lDv,&D);
  //VecGetArray2d(Dv,(yw+2),(xw+2),ys-1,xs-1,&D);
  VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  PetscMemzero(col,5*sizeof(MatStencil));
  
  for (j = ys; j < ys+yw; j++){
  for (i = xs; i < xs+xw; i++){
  
    ap  = -2.0*(dx*dx+dy*dy);
    aex = dy*dy;
    awx = dy*dy;
    aey = dx*dx;
    awy = dx*dx;
      
    duud2x = ( L[j  ][i-1].u*L[j  ][i-1].u - 2.0*L[j][i].u*L[j][i].u + L[j  ][i+1].u*L[j  ][i+1].u )/(dx*dx); // d2(u^2)/dx2
    dvvd2y = ( L[j-1][i  ].v*L[j-1][i  ].v - 2.0*L[j][i].v*L[j][i].v + L[j+1][i  ].v*L[j+1][i  ].v )/(dy*dy); // d2(v^2)/dy2
    duvdxy = (1.0/(dx*dy))*( uxN[j+1][i+1][1]*vyN[j+1][i+1][1]   // uv at i+1/2,j+1/2
                           - uxN[j+1][i  ][1]*vyN[j+1][i  ][1]   // uv at i-1/2,j+1/2
                           - uxN[j  ][i+1][1]*vyN[j  ][i+1][1]   // uv at i+1/2,j-1/2
                           + uxN[j  ][i  ][1]*vyN[j  ][i  ][1]); // uv at i-1/2,j-1/2
    
    D2x = ( D[j  ][i-1] - 2.0*D[j][i] + D[j  ][i+1] )/(dx*dx); // d2D/dx2
    D2y = ( D[j-1][i  ] - 2.0*D[j][i] + D[j+1][i  ] )/(dy*dy); // d2D/dy2

    rhs = (dx*dx*dy*dy)*( D[j][i]/dt - duud2x - dvvd2y - 2.0*duvdxy + (1.0/Re)*( D2x + D2y ) );  
    
    vals[0] = ap;
    vals[1] = awx;
    vals[2] = aex;
    vals[3] = awy;
    vals[4] = aey;
    b[j][i] = rhs;

    // Specify matrix location of elements using (i,j) location in grid	
    row.i    = i  ;    row.j = j  ; // matrix row
    col[0].i = i  ; col[0].j = j  ; // ap
    col[1].i = i-1; col[1].j = j  ; // awx
    col[2].i = i+1; col[2].j = j  ; // aex
    col[3].i = i  ; col[3].j = j-1; // awy 
    col[4].i = i  ; col[4].j = j+1; // aey
    MatSetValuesStencil(A,1,&row,5,col,vals,ADD_VALUES);    
  
  }
  }
    
  DMDAVecRestoreArray(da,lv,&L);
  DMDAVecRestoreArray(daKSP,bv,&b);
  DMDAVecRestoreArray(daKSP,lDv,&D);
  //VecRestoreArray2d(Dv,(yw+2),(xw+2),ys-1,xs-1,&D);
  VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  
  MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
  VecAssemblyBegin(bv);
  VecAssemblyEnd(bv);
  //VecView(bv,PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A,PETSC_VIEWER_STDOUT_WORLD);
  KSPSolve(ksp,bv,X);

  DMGlobalToLocalBegin(daKSP,X,INSERT_VALUES,lX);
  DMGlobalToLocalEnd(daKSP,X,INSERT_VALUES,lX);
  VecStrideScatter(lX,5,lv,INSERT_VALUES); // scatter into pressure slot
  MatZeroEntries(A);
  
}

void compute_face_vel(){
  PetscInt	i,j;
  
  VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  DMDAVecGetArray(da,lv,&L);
  for (j = ys; j < ys+yw+1; j++){ 
  for (i = xs; i < xs+xw+1; i++){
  	uxN[j][i][1]=0.5*(L[j  ][i-1].u+L[j][i].u);
  	vyN[j][i][1]=0.5*(L[j-1][i  ].v+L[j][i].v);
  }
  }
  VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  DMDAVecRestoreArray(da,lv,&L);
  
}

void update_cv_mom_and_vel(){
  PetscInt i,j;
  
}

void store_face_vel(){
  PetscInt i,j;
  
  VecGetArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecGetArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  for (j = ys; j < ys+yw+1; j++){ 
  for (i = xs; i < xs+xw+1; i++){
  	uxN[j][i][0]=uxN[j][i][1];
  	vyN[j][i][0]=vyN[j][i][1];
  }
  }
  VecRestoreArray3d(uxNv,(yw+1),(xw+1),2,ys,xs,0,&uxN);
  VecRestoreArray3d(vyNv,(yw+1),(xw+1),2,ys,xs,0,&vyN);
  
}

// this function computes du/dx + du/dy
void compute_duxduy(){
  PetscInt i,j;
  PetscReal dux,duy;
  
  DMDAVecGetArray(da,lv,&L);
  DMDAVecGetArray(daKSP,Dv,&D);
    
    // interior points
    if(xs != 0 && ys != 0 &&  xs+xw != nx && ys+xw != ny){
      for (j = ys-1; j < ys+yw+1; j++){ 
      for (i = xs-1; i < xs+xw+1; i++){
        dux = 0.5*(L[j  ][i+1].u-L[j  ][i-1].u)/dx; // central
        duy = 0.5*(L[j+1][i  ].v-L[j-1][i  ].v)/dy; // central
        D[j][i] = dux+duy;
      }
      }
    }
  
    /*
    // boundary i == -1
    if(xs==0){
      for (j=ys; j<ys+yw; j++){
        i = -1;
        dux = 1.0*(L[j  ][i+1].u-L[j  ][i  ].u)/dx; // forward 
        duy = 0.5*(L[j+1][i  ].v-L[j-1][i  ].v)/dy; // central 
        D[j][i] = dux+duy;
      }
    }
    
    // boundary j == -1   
    if(ys==0){
      for (i=xs; i<xs+xw; i++){
        j = -1;
        dux = 0.5*(L[j  ][i+1].u-L[j  ][i-1].u)/dx; // central
        duy = 1.0*(L[j+1][i  ].v-L[j  ][i  ].v)/dy; // forward
        D[j][i] = dux+duy;
      }
    }
    
    // boundary i == nx
    if(xs+xw==nx){
      for (j=ys; j<ys+yw; j++){
        i = nx;
        dux = 1.0*(L[j  ][i  ].u-L[j  ][i-1].u)/dx; // backward 
        duy = 0.5*(L[j+1][i  ].v-L[j-1][i  ].v)/dy; // central 
        D[j][i] = dux+duy;
      } 
    }
   
    // boundary j == ny
    if(ys+yw==ny){
      for (i=xs; i<xs+xw; i++){
        j = ny;
        dux = 0.5*(L[j  ][i+1].u-L[j  ][i-1].u)/dx; // central
        duy = 1.0*(L[j  ][i  ].v-L[j-1][i  ].v)/dy; // backward
        D[j][i] = dux+duy;
      }
    }
  */    
  DMDAVecRestoreArray(da,lv,&L);
  DMDAVecRestoreArray(daKSP,Dv,&D);

  DMGlobalToLocalBegin(daKSP,Dv,INSERT_VALUES,lDv);
  DMGlobalToLocalEnd(daKSP,Dv,INSERT_VALUES,lDv);
  //VecRestoreArray2d(Dv,(yw+2),(xw+2),ys-1,xs-1,&D);
  
  //VecView(Dv,PETSC_VIEWER_STDOUT_SELF);
  
}

void taylor_pressure_exact(){
  PetscInt	  i,j;
    
  DMDAVecGetArray(da,lv,&L);
  VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
  
  for (j = ys; j < ys+yw; j++){
  for (i = xs; i < xs+xw; i++){
    L[j][i].p = -0.25*(PetscCosReal(2.0*x[j][i])+PetscCosReal(2.0*y[j][i]))*PetscExpReal(((-4.0/Re)*sim_time));
  }
  }

/*
  if(xs==0){
    for(j = ys; j < ys+yw; j++){
      L[j][0].p = L[j][1].p; 
    }
  }
  if(ys==0){
    for(i = xs; i < xs+xw; i++){
      L[0][i].p = L[1][i].p;
    }
  }
  if(xs+xw==nx){
    for(j = ys; j < ys+yw; j++){
      L[j][nx].p = L[j][nx-1].p;
    }
  }
  if(ys+yw==ny){
    for(i = xs; i < xs+xw; i++){
      L[ny][i].p = L[ny-1][i].p;
    }
  }
*/
  DMDAVecRestoreArray(da,lv,&L);
  VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
  VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);

}
void time_forward(){
  sim_time += dt;
}

void save_data(){
	FILE *file = NULL;
	char filename[PETSC_MAX_PATH_LEN];
	PetscInt i,j,k,zone;  
	PetscReal zp;
	//PetscViewer viewer;
	if(num_save==0){
		zone=0;
	} else {
		zone=n/save_interval;
	}
	if(yes_tecplot==1){
		PetscSNPrintf(filename,sizeof(filename),"data/XX%04i.%04i.dat",num_save,rank);
		PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
		PetscFPrintf(PETSC_COMM_SELF,file,"Title=\"Jet\"\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v,gx,gy,p\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\", STRANDID=1, SOLUTIONTIME=%f I=%i J=%i K=4 F=POINT\n",zone,sim_time,xw+2,yw+2);
		
		DMDAVecGetArray(da,lv,&L);
		VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
		VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
		for (k = 0; k < 4; k++){
		for (j = ys-1; j < ys+yw+1; j++){
		for (i = xs-1; i < xs+xw+1; i++){
			zp=k*0.5+0.5/2.0;		
			PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",x[j][i],y[j][i],zp,L[j][i].d,L[j][i].u,L[j][i].v,L[j][i].gx,L[j][i].gy,L[j][i].p);
		}		
		}
		}
		DMDAVecRestoreArray(da,lv,&L);
		VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
		VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
		fclose(file);
		file = NULL;
	}
	if(yes_tecplot==0){
		PetscSNPrintf(filename,sizeof(filename),"data/YY%04i.%04i.dat",rank,num_save);
		PetscFOpen(PETSC_COMM_SELF,filename,"w",&file);
		PetscFPrintf(PETSC_COMM_SELF,file,"variables=x,y,z,rho,u,v,gx,gy,p,D\n");	
		PetscFPrintf(PETSC_COMM_SELF,file,"Zone T=\"%i\",I=%i J=%i K=4 F=POINT\n",zone,xw+2,yw+2);
		
		DMDAVecGetArray(da,lv,&L);
        DMDAVecGetArray(daKSP,lDv,&D);
        //VecGetArray2d(Dv,(yw+2),(xw+2),ys-1,xs-1,&D);
		VecGetArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
		VecGetArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
		for (k = 0; k < 4; k++){
		for (j = ys-1; j < ys+yw+1; j++){
		for (i = xs-1; i < xs+xw+1; i++){
			zp=k*0.5+0.5/2.0;		
			PetscFPrintf(PETSC_COMM_SELF,file,"%15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e %15.6e\n",x[j][i],y[j][i],zp,L[j][i].d,L[j][i].u,L[j][i].v,L[j][i].gx,L[j][i].gy,L[j][i].p,D[j][i]);
		}		
		}
		}
		DMDAVecRestoreArray(da,lv,&L);
        DMDAVecRestoreArray(daKSP,lDv,&D);
        //VecRestoreArray2d(Dv,(yw+2),(xw+2),ys-1,xs-1,&D);
		VecRestoreArray2d(xv,(yw+2),(xw+2),ys-1,xs-1,&x);
		VecRestoreArray2d(yv,(yw+2),(xw+2),ys-1,xs-1,&y);
		fclose(file);
		file = NULL;
	}
	num_save=num_save+1;
};

